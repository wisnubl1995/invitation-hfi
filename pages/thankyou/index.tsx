// pages/register.js

import { useState } from "react";

import Image from "next/image";
import hero from "@/public/img/hero.png";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Head from "next/head";
import logo from "@/public/img/logohfi.png";
import text1 from "@/public/img/tq1.png";
import text2 from "@/public/img/tq2.png";
import text3 from "@/public/img/text3.png";
import text4 from "@/public/img/text4.png";
import h1 from "@/public/img/h1.png";
import p from "@/public/img/p.png";

export default function Register() {
  const [fullName, setFullName] = useState("");
  const [companyName, setCompanyName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [email, setEmail] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const [isRegister, setIsRegister] = useState(false);

  const handleSubmit = async (e: any) => {
    setIsLoading(true);
    e.preventDefault();
    try {
      const response = await fetch("/api/register", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          fullName,
          companyName,
          phoneNumber,
          email,
        }),
      });
      if (response.ok) {
        console.log("User registered successfully!");
        toast.success("Registration successful!");
        const userData = {
          fullName,
          companyName,
          phoneNumber,
          email,
        };
        localStorage.setItem("userData", JSON.stringify(userData));
        setTimeout(() => {
          window.location.reload();
        }, 2000);
      } else {
        console.error("Error registering user:", response.statusText);
        toast.error("Error registering user!");
      }
    } catch (error) {
      console.error("Error registering user:", error);
      toast.error("Error registering user!");
    } finally {
      setIsLoading(false); // Set loading status back to false after request completes
    }
  };

  return (
    <div className="max-w-[480px] mx-auto flex flex-col h-[100vh] bg-main bg-no-repeat bg-cover">
      <Head>
        <title>Invitation HFI</title>
      </Head>
      {/* Hero Section */}
      <ToastContainer />
      {/* Registration Form */}
      <div className="w-full mx-auto text-white p-5">
        <>
          <div className="flex flex-col items-center  gap-10  pt-36">
            <div className="w-full flex flex-col items-center justify-center">
              <div className="w-full font-sans  flex flex-col items-center">
                <div className="w-full flex flex-col items-center">
                  <div className="w-12/12">
                    <Image className="" src={logo} alt="logo" />
                  </div>
                  <div className="pt-4">
                    <Image src={hero} alt="logo" />
                  </div>
                </div>
              </div>
              <div className="pt-16 w-full flex flex-col items-center ">
                <div className="w-full pb-1 flex flex-col items-center">
                  <h1 className="lg:text-[24px] text-[14px] text-center leading-6 tracking-tight">We are profoundly regret that you unable to attend <br />  the 10th Anniversary of PT Hino Finance Indonesia.</h1>
                </div>
                <div className="w-full pb-1 flex flex-col items-center pt-8">
                  <p className="lg:text-[20px] text-[14px] text-center leading-6 tracking-tighter">Nonetheless, we sincerely appreciate your response and <br /> kindly request your best wishes for HFI. We hope to have <br /> the opportunity to meet at another valuable occasion.</p>
                </div>
              </div>
            </div>
          </div>
        </>
      </div>
    </div>
  );
}
