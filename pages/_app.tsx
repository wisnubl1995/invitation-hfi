import "@/styles/globals.css";
import type { AppProps } from "next/app";
import localFont from "@next/font/local";
import { appWithTranslation } from "next-i18next";
import "../i18n/i18n";

const poppins = localFont({
  src: [
    {
      path: "../public/font/perpeta.ttf",
      weight: "400",
    },
  ],
  variable: "--font-poppins",
});

function App({ Component, pageProps }: AppProps) {
  return (
    <div className={`${poppins.variable}`}>
      <Component {...pageProps} />
    </div>
  );
}

export default appWithTranslation(App);
