// pages/register.js

import { useState } from "react";

import Image from "next/image";
import hero from "@/public/img/hero.png";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Head from "next/head";
import logo from "@/public/img/logohfi.png";
import text1 from "@/public/img/text1.png";
import text2 from "@/public/img/text2.png";
import text3 from "@/public/img/text3.png";
import text4 from "@/public/img/text4.png";
import h1 from "@/public/img/h1.png";
import p from "@/public/img/p.png";

export default function Register() {
  const [fullName, setFullName] = useState("");
  const [companyName, setCompanyName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [email, setEmail] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const [isRegister, setIsRegister] = useState(false);
  //40, 43, 44, 46
  return (
    <div className="max-w-[480px] mx-auto flex flex-col h-[100vh] bg-main bg-no-repeat bg-cover">
      <Head>
        <title>Invitation HFI</title>
      </Head>
      {/* Hero Section */}
      <ToastContainer />
      {/* Registration Form */}
      <div className="w-full mx-auto text-white p-3">
        <>
          <div className="flex flex-col items-center  gap-5 h-[80vh] justify-center pt-36">
            <div className="w-full flex flex-col items-center justify-center">
              <div className="w-full font-sans  flex flex-col items-center">
                <div className="w-full flex flex-col items-center">
                  <div className="w-12/12">
                    <Image className="" src={logo} alt="logo" />
                  </div>
                  <div className="pt-4">
                    <Image src={hero} alt="logo" />
                  </div>
                </div>
              </div>
              <div className="pt-10 w-full flex flex-col items-center">
                <div className="w-full pb-1 flex flex-col items-center font-bold text-center">
                  <h1 className="lg:text-[60px] text-[30px] uppercase font-bold ignore-darkmode">
                    Thank You
                  </h1>
                  <h1 className="lg:text-[20px] lg:-mt-5 mt-0 text-[10px] uppercase font-bold ignore-darkmode">
                    For being our guest of honor
                  </h1>
                </div>

                <div className="w-full pb-1 flex flex-col items-center pt-2 text-center lg:text-[18px] text-[10px]">
                  <p>
                    Following your reservation, we will be sending you <br />{" "}
                    the e-invitation soon and when the event is approaching{" "}
                    <br /> it will require a final confirmation from you.
                  </p>
                </div>
                <div className="w-full pb-1 flex flex-col items-center pt-5 text-center lg:text-[18px] text-[10px]">
                  <p>
                    We look forward to your presence on <br />
                    the 10th Anniversary of PT Hino Finance Indonesia.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </>
      </div>
    </div>
  );
}
