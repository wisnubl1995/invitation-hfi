import { useTranslation } from "react-i18next";
import { appWithTranslation } from "next-i18next";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Image from "next/image";
import hero from "@/public/img/newHero.png";
import "react-toastify/dist/ReactToastify.css";
import logo from "@/public/img/newLogo.png";
import idLogo from "@/public/img/id.png";
import enLogo from "@/public/img/en.png";
import rossa from "@/public/img/rossa.png";
import rossaText from "@/public/img/rossaText.png";
import cowok1 from "@/public/img/cowok1.png";
import cowok2 from "@/public/img/cowok2.png";
import cewek1 from "@/public/img/cewek1.png";
import cewek2 from "@/public/img/cewek2.png";
import border from "@/public/img/Group.png";
import Head from "next/head";
import Link from "next/link";
import CountdownTimer from "@/components/CountdownTimer";
import Agendabox from "@/components/Agendabox";
// import Swiper core and required modules
import { Navigation, Pagination, Scrollbar, A11y } from "swiper/modules";

import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";

type Props = {
  fullName: string;
  companyName: string;
  phoneNumber: string;
  email: string;
  seat: string;
};

export async function getServerSideProps(context: any) {
  const { locale, query } = context;
  const { id } = query;

  try {
    const response = await fetch(
      `https://rsvp-hfi10th-anniversary.vercel.app/api/fetch-single-user?phoneNumber=${encodeURIComponent(
        id
      )}`
    );
    if (response.ok) {
      const user = await response.json();

      return {
        props: {
          initialUser: user,
        },
      };
    } else {
      return {
        props: { error: response.statusText },
      };
    }
  } catch (error: any) {
    return {
      props: { error: error.message },
    };
  }
}

const Index = ({ initialUser }: any) => {
  const { t, i18n } = useTranslation();
  const [user, setUser] = useState(initialUser);
  const [users, setUsers] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [indonesia, setIndonesia] = useState(false);
  const [english, setEnglish] = useState(true);
  const [pakaian, setPakaian] = useState("PRIA");
  const router = useRouter();
  const { phoneNumber }: any = router.query;
  const [isInternal, setIsInternal] = useState(false);

  const spaceRegex = /-/g;

  console.log(user);

  const fetchUsers = async () => {
    try {
      const response = await fetch(
        `/api/fetch-single-user?phoneNumber=${encodeURIComponent(phoneNumber)}`
      );
      if (response.ok) {
        const data = await response.json();
        setUsers(data);
      } else {
        console.error("Failed to fetch users:", response.statusText);
      }
    } catch (error) {
      console.error("Failed to fetch users:", error);
    }
  };

  const phoneNumberWithSpace = phoneNumber?.replace(spaceRegex, " ");

  const images = [
    {
      id: 1,
      image: cowok1,
      type: "PRIA",
      description:
        "Kemeja Batik Lengan Panjang dengan Celana Formal dan Sepatu Formal",
    },

    {
      id: 4,
      image: cewek2,
      type: "WANITA",
      description: "kebaya formal dan heels",
    },
  ];

  const saveQRToDevice = () => {
    const link = document.createElement("a");
    link.href = user.qrConfirm; // QR image source
    link.download = "QR_Code.png"; // Download file name
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  const filteredImages = images.filter((image) => image.type === pakaian);
  const changeLanguage = (lng: string) => {
    i18n.changeLanguage(lng);
  };

  return (
    <div className="max-w-[480px] mx-auto flex flex-col h-full justify-center items-center bg-bgNew  bg-cover">
      <Head>
        <title>E-Invitation {user.fullName}</title>
      </Head>

      <div className="flex flex-col items-center  gap-10 pt-10 text-white font-sans">
        <div className="w-full flex flex-col items-center">
          <div className="pt-6 w-8/12 flex justify-center">
            <Image fetchPriority="auto" src={logo} alt="logo" />
          </div>
          <div className="pt-3">
            <h3 className="text-[15px] tracking-wide">Dear Mr./Mrs.</h3>
          </div>
          <div>
            <h1 className="text-[24px]">{user.fullName}</h1>
          </div>

          <div className="h-[1px] w-8/12 bg-white -mt-2"></div>
          {user.isInternal == true ? (
            <></>
          ) : (
            <div>
              <h1 className="text-[18px]">{user.companyName}</h1>
            </div>
          )}
          <div className="pt-4 w-full">
            <Image
              src={hero}
              className="w-full"
              alt="logo"
              width={1000}
              height={1000}
            />
          </div>

          <div className="pt-3 flex justify-center gap-3 text-black  uppercase">
            <div
              onClick={() => {
                setIndonesia(true), setEnglish(false), changeLanguage("id");
              }}
              className={`w-[133px] h-[25px] rounded-full flex justify-center items-center
                ${indonesia == true ? "bg-white" : "bg-white/25"}
                `}
            >
              <div className="w-2/12 flex items-center justify-center">
                <Image className="w-8/12" src={idLogo} alt="id" />
              </div>
              <div className="w-9/12 flex items-center justify-center">
                <h3 className="text-[10px]">Bahasa Indonesia</h3>
              </div>
            </div>
            <div
              onClick={() => {
                setIndonesia(false), setEnglish(true), changeLanguage("en");
              }}
              className={`w-[133px] h-[25px] rounded-full flex justify-center items-center
                ${english == true ? "bg-white" : "bg-white/25"}
                `}
            >
              <div className="w-2/12 flex items-center justify-center">
                <Image className="w-8/12" src={enLogo} alt="id" />
              </div>
              <div className="w-9/12 flex items-center justify-center">
                <h3 className="pr-3 text-[12px]">English</h3>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="flex flex-col items-center py-3  mt-6 bg-[#5C0406] shadow-box w-10/12 text-white rounded-xl">
        <div className="mt-3">
          <h1 className="text-[24px] font-bold">{t("date")}</h1>
        </div>

        <div className="flex items-center justify-center mt-1 gap-5">
          <div className="flex flex-col items-center">
            <div>
              <h3 className="text-[20px]">{t("start")}:</h3>
            </div>

            <div className="h-[1px] w-[100px] bg-white"></div>

            <div>
              <h3 className="text-[20px]">18:00 WIB</h3>
            </div>
          </div>
          <div className="flex flex-col items-center">
            <div>
              <h3 className="text-[20px]">{t("end")}:</h3>
            </div>

            <div className="h-[1px] w-[100px] bg-white"></div>

            <div>
              <h3 className="text-[20px]">21:30 WIB</h3>
            </div>
          </div>
        </div>

        <div className="mt-5 flex flex-col items-center">
          <h3 className="text-[26px] font-bold tracking-wider">
            Park Hyatt Hotel
          </h3>
          <h3 className="text-[17px] font-bold tracking-wider">
            Royal Glasshouse
          </h3>
          <p className="text-center text-[11px] tracking-wide mt-3 w-full">
            MNC Tower, Jl Kebon Sirih 17-18, Menteng, Central <br /> Jakarta,
            DKI Jakarta, Indonesia
          </p>
        </div>

        <div className="mt-5 w-[150px] h-[36px] bg-[#DADADA] rounded-full flex justify-center items-center">
          <Link
            className="text-black uppercase"
            href={"https://maps.app.goo.gl/kr8eKcSBNq7BjsgBA"}
          >
            {t("maps")}
          </Link>
        </div>
      </div>

      <CountdownTimer />

      <Agendabox />

      <div className="mt-5 text-white flex flex-col items-center">
        <div className="text-center">
          <h1 className="uppercase text-[26px]">{t("performance")}</h1>
        </div>
        <div className="w-10/12 relative mt-3 flex justify-center">
          <Image className="w-full" src={rossa} alt="rossa" />
          <Image
            className="w-6/12 absolute bottom-4"
            src={rossaText}
            alt="rossa"
          />
        </div>
      </div>

      {user.isInternal == true ? (
        <>
          <div className="mt-5 text-white flex flex-col items-center">
            <div className="text-center">
              <h1 className="uppercase text-[26px]">{t("dress")}</h1>
            </div>
            <div className="flex justify-center gap-5 mt-3">
              <div
                onClick={() => {
                  setPakaian("PRIA");
                }}
                className={`w-[130px] h-[28px] flex justify-center rounded-full
          ${pakaian == "PRIA" ? "bg-[#DADADA]" : "bg-[#DADADA]/40"}
          `}
              >
                <h3 className="uppercase text-black mt-[2px]">{t("male")}</h3>
              </div>
              <div
                onClick={() => {
                  setPakaian("WANITA");
                }}
                className={`w-[130px] h-[28px] flex justify-center rounded-full
              ${pakaian == "WANITA" ? "bg-[#DADADA]" : "bg-[#DADADA]/40"}
              `}
              >
                <h3 className="uppercase text-black mt-[2px]">{t("female")}</h3>
              </div>
            </div>
          </div>
          <div className="mt-5 flex flex-col items-center w-full px-3 relative">
            <Swiper
              modules={[Navigation, Pagination, Scrollbar, A11y]}
              spaceBetween={10}
              slidesPerView={1}
              navigation
              pagination={{ clickable: true }}
              className="w-full max-w-3xl flex justify-center items-center swiper-custom h-[100vw] lg:h-[50vh] md:h-[50vh]"
            >
              {filteredImages.map((image) => (
                <SwiperSlide key={image.id} className="">
                  <div className="flex justify-center items-center mt-5">
                    <Image
                      src={image.image}
                      alt={`Image ${image.id}`}
                      className="w-10/12 h-auto object-cover"
                      width={1000}
                      height={1000}
                    />
                  </div>
                </SwiperSlide>
              ))}
            </Swiper>
            <div className="text-white text-center absolute bottom-12 uppercase text-[13px] w-10/12">
              {pakaian == "PRIA" ? (
                <h3>{t("maleDesc")}</h3>
              ) : (
                <h3>{t("femaleDesc")}</h3>
              )}
            </div>
          </div>
        </>
      ) : (
        <>
          <div className="flex flex-col items-center py-5 mt-10 mb-10 bg-[#5C0406] shadow-box w-10/12 text-white rounded-xl">
            <div>
              <h1 className="uppercase text-[24px]">{t("dress")}</h1>
            </div>
            <div className="h-[1px] w-[280px] bg-white"></div>
            <div>
              <h1 className="uppercase text-[18px] mt-1">Formal Attire</h1>
            </div>
          </div>
        </>
      )}

      <div className="flex flex-col pb-12 mt-5 items-center w-full">
        <div className="text-center flex justify-center">
          <h3 className=" text-[16px] text-white w-8/12">{t("qr")}</h3>
        </div>
        <div className="w-full flex justify-center mt-3">
          <div className="bg-qrCode rounded-xl flex justify-center items-center w-[252px] h-[252px] relative">
            <Image
              className="absolute"
              src={border}
              alt="broder"
              data-fetchpriority="auto" // Menggunakan data-fetchpriority untuk properti HTML kustom
            />

            {user.qrConfirm ? (
              <img
                className="w-[140px] h-[140px]"
                src={`${user.qrConfirm}`}
                alt="qrCode"
                width={160}
                height={160}
                data-fetchpriority="auto"
              />
            ) : (
              <div>QR Code tidak tersedia</div>
            )}
          </div>
        </div>

        <div className="text-center mt-3 flex justify-center">
          <h3 className=" text-[16px] text-white w-10/12">{t("thankyou")}</h3>
        </div>
      </div>
    </div>
  );
};

export default Index;
