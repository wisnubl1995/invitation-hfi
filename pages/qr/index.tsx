import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Image from "next/image";
import hero from "@/public/img/hero.png";
import "react-toastify/dist/ReactToastify.css";
import logo from "@/public/img/logohfi.png";
import border from "@/public/img/Group.png";
import Head from "next/head";

type Props = {
  fullName: string;
  companyName: string;
  phoneNumber: string;
  email: string;
  seat: string;
};

export async function getServerSideProps(context: any) {
  const { query } = context;
  const { phoneNumber } = query;

  try {
    const response = await fetch(
      `https://invitation-hfi.vercel.app/api/fetch-single-user?phoneNumber=${encodeURIComponent(
        phoneNumber
      )}`
    );
    if (response.ok) {
      const user = await response.json();

      return {
        props: {
          initialUser: user,
        },
      };
    } else {
      return {
        props: { error: response.statusText },
      };
    }
  } catch (error: any) {
    return {
      props: { error: error.message },
    };
  }
}

const Index = ({ initialUser }: any) => {
  const [user, setUser] = useState(initialUser);
  const [users, setUsers] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const router = useRouter();
  const { fullName, companyName, phoneNumber, email, seat }: any = router.query;

  const spaceRegex = /-/g;

  const fetchUsers = async () => {
    try {
      const response = await fetch(
        `/api/fetch-single-user?phoneNumber=${encodeURIComponent(phoneNumber)}`
      );
      if (response.ok) {
        const data = await response.json();
        setUsers(data);
      } else {
        console.error("Failed to fetch users:", response.statusText);
      }
    } catch (error) {
      console.error("Failed to fetch users:", error);
    }
  };

  const fullNameWithSpace = fullName?.replace(spaceRegex, " ");
  const companyNameWithSpace = companyName?.replace(spaceRegex, " ");
  const phoneNumberWithSpace = phoneNumber?.replace(spaceRegex, " ");
  const emailWithSpace = email?.replace(spaceRegex, " ");
  const seatWithSpace = seat?.replace(spaceRegex, " ");

  useEffect(() => {
    const interval = setInterval(() => {
      fetchUsers();
    }, 1000);

    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    if (user && user.hadir === "Yes") {
      router.push(
        `/last?fullName=${fullName}&companyName=${companyName}&phoneNumber=${phoneNumber}&email=${email}&seat=${seat}`
      );
    }
  }, [user]);

  const saveQRToDevice = () => {
    const link = document.createElement("a");
    link.href = user.qrConfirm; // QR image source
    link.download = "QR_Code.png"; // Download file name
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  return (
    <div className="max-w-[480px] mx-auto flex flex-col h-[100vh] justify-center items-center bg-main bg-no-repeat bg-cover">
      <Head>
        <title>QR</title>
      </Head>
      <div className="flex flex-col items-center  gap-10 pt-10">
        <div className="w-full flex flex-col items-center">
          <div className="w-10/12 flex justify-center">
            <Image fetchPriority="auto" src={logo} alt="logo" />
          </div>
          <div className="pt-1 w-10/12">
            <Image src={hero} fetchPriority="auto" alt="logo" />
          </div>
        </div>
      </div>
      <div className="px-8 pt-6 pb-8 mb-4 flex flex-col items-center w-full">
        <div className="mb-4 w-full flex justify-center">
          <div className="bg-qrCode rounded-xl flex justify-center items-center w-[252px] h-[252px] relative">
            <Image
              className="absolute"
              src={border}
              alt="broder"
              data-fetchpriority="auto" // Menggunakan data-fetchpriority untuk properti HTML kustom
            />
            <Image
              className="w-[140px] h-[140px]"
              src={`${user.qrConfirm}`}
              alt="qrCode"
              width={160}
              height={160}
              data-fetchpriority="auto" // Menggunakan data-fetchpriority untuk properti HTML kustom
            />
          </div>
        </div>

        <div className="flex justify-center pt-20 text-white">
          <button
            type="button"
            className=" font-sans px-6 py-1 bg-gradient-to-b from-[#D9D9D9] to-[#737373] border-white border-2 rounded-full w-full text-[20px] font-bold tracking-wider hover:bg-gradient-to-b hover:from-[#737373] hover:to-[#D9D9D9] transition-all ease-out duration-200"
            onClick={saveQRToDevice} // Call saveQRToDevice function on button click
            disabled={isLoading}
          >
            {isLoading ? "Loading..." : "SAVE TO DEVICE"}
          </button>
        </div>
      </div>
    </div>
  );
};

export default Index;
