// pages/index.tsx
import { useTranslation } from "react-i18next";

export default function Index() {
  const { t, i18n } = useTranslation();

  const changeLanguage = (lng: string) => {
    i18n.changeLanguage(lng);
  };

  return (
    <div className="container mx-auto p-4">
      <h1 className="text-2xl font-bold">{t("welcome")}</h1>
      <button
        className="px-4 py-2 m-2 bg-blue-500 text-white rounded"
        onClick={() => changeLanguage("en")}
      >
        English
      </button>
      <button
        className="px-4 py-2 m-2 bg-green-500 text-white rounded"
        onClick={() => changeLanguage("id")}
      >
        Bahasa Indonesia
      </button>
    </div>
  );
}
