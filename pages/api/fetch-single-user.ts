// pages/api/fetch-user-by-phoneNumber.js

import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

export default async function handler(req: any, res: any) {
  const { phoneNumber } = req.query;

  try {
    const user = await prisma.user.findUnique({
      where: {
        phoneNumber: phoneNumber,
      },
      select: {
        // Menentukan field yang akan diambil
        id: true,
        fullName: true,
        email: true,
        phoneNumber: true,
        companyName: true,
        registrationAttendance: true,
        numberOfPax: true,
        isAllergic: true,
        sendAttendance: true,
        confirmAttendance: true,
        seatNumber: true,
        qrCode: true,
        qrConfirm: true,
        url: true,
        urlConrim: true,
        hadir: true,
        allergicInformationIDs: true,
        isInternal: true, // Pastikan field ini disertakan
        newData: true,
      },
    });

    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    res.status(200).json(user);
  } catch (error) {
    console.error("Failed to fetch user:", error);
    res.status(500).json({ message: "Internal Server Error" });
  }
}
