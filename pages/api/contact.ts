import { NextApiRequest, NextApiResponse } from "next";
const nodemailer = require("nodemailer");

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { fullName, email, phoneNumber, subject, message, companyName } =
    req.body;

  const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true,
    auth: {
      user: "no-reply@pk-ent.com",
      pass: "tmtzeeeedfxagcvf",
    },
  });

  const recipients = [email];

  const info = await transporter.sendMail({
    from: `"no-reply@pk-ent.com" <${email}>`,
    to: recipients.join(", "),
    subject: "Attendance Confirmation",
    text: "Please confirm your attendance.",
    html: `
    <h1>Thank you!</h1>
    <p>Dear ${fullName},</p>
    <br />
    <p>We like you to confirm your attendance</p>
    <p>Name: ${fullName}<br />Company: ${companyName}<br />Phone Number: ${phoneNumber}<br /></p>
    <p>Confirmation Link</p>
    <p>https://invitation-hfi.vercel.app/confirmation</p>
    <br />
    <p>Please dont reply this message, our team will contact you as soon as possible, thank you!</p>
    `,
  });

  console.log("Message sent: %s", info.messageId);

  res.status(200).json({ success: true });
}
