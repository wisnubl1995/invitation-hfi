import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

export default async function handler(req: any, res: any) {
  if (req.method !== "GET") {
    return res.status(405).json({ message: "Method Not Allowed" });
  }

  try {
    const userId = req.query.userId; // Ambil userId dari query parameter
    const user = await prisma.user.findUnique({
      where: {
        id: userId,
      },
      include: {
        allergicInformations: true, // Mengambil allergicInformationIDs dari User
      },
    });

    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    const allergicInfoList = [];

    // Loop melalui allergicInformationIDs dan mencetak informasi dari setiap AllergicInfo
    for (const allergicInfoId of user.allergicInformationIDs) {
      const allergicInfo = await prisma.allergicInfo.findUnique({
        where: {
          id: allergicInfoId,
        },
      });

      if (!allergicInfo) {
        console.log(`AllergicInfo with ID ${allergicInfoId} not found`);
        continue;
      }

      // Tambahkan informasi dari setiap AllergicInfo ke dalam array
      allergicInfoList.push({
        name: allergicInfo.name,
        allergic: allergicInfo.allergic,
      });
    }

    return res.status(200).json(allergicInfoList);
  } catch (error) {
    console.error("Error fetching allergic information:", error);
    return res.status(500).json({ message: "Internal Server Error" });
  }
}
