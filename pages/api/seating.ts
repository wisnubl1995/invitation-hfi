import { NextApiRequest, NextApiResponse } from "next";
import fs from "fs";
import path from "path";
const nodemailer = require("nodemailer");

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const {
    fullName,
    email,
    phoneNumber,
    subject,
    message,
    companyName,
    qrCode,
    url,
  } = req.body;

  const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true,
    auth: {
      user: "no-reply@pk-ent.com",
      pass: "tmtzeeeedfxagcvf",
    },
  });

  const recipients = [email];

  const info = await transporter.sendMail({
    from: `"no-reply@pk-ent.com" <${email}>`,
    to: recipients.join(", "),
    subject: "Seating QR",
    text: "Seating QR",
    html: `
    <h1>Thank you!</h1>
    <p>Dear ${fullName},</p>
    <br />
    <p>This is your detail and QR Code</p>
    <p>Name: ${fullName}<br />Company: ${companyName}<br />Phone Number: ${phoneNumber}<br /></p>
    <p>QR Code</p>
    <img style="width:400px" src="${qrCode}" alt="QR-${fullName}" />
    <p>Jika QR tidak keluar, bisa klik url dibawah ini</p>
    <a href="${url}">Klik Untuk Melihat QR Code</a>
    
    <br />
    <p>Please dont reply this message, our team will contact you as soon as possible, thank you!</p>
    `,
  });

  console.log("Message sent: %s", info.messageId);

  res.status(200).json({ success: true });
}
