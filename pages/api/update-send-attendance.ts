// pages/api/update-send-attendance.js

import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

export default async function handler(req: any, res: any) {
  if (req.method !== "POST") {
    return res.status(405).json({ message: "Method Not Allowed" });
  }

  const { phoneNumber } = req.body;

  try {
    const updatedUser = await prisma.user.update({
      where: { phoneNumber },
      data: { sendAttendance: true },
    });

    res.status(200).json({ success: true, updatedUser });
  } catch (error) {
    console.error("Failed to update sendAttendance:", error);
    res.status(500).json({ success: false, message: "Internal Server Error" });
  }
}
