import { PrismaClient } from "@prisma/client";
import QRCode from "qrcode";

const prisma = new PrismaClient();

export default async function handler(req: any, res: any) {
  if (req.method !== "POST") {
    return res.status(405).json({ message: "Method Not Allowed" });
  }

  const {
    fullName,
    companyName,
    email,
    phoneNumber,
    seatNumber,
    protocolAndDomain,
    attendance,
  } = req.body;

  try {
    const existingUser = await prisma.user.findUnique({
      where: { phoneNumber },
    });

    if (!existingUser) {
      return res
        .status(404)
        .json({ success: false, message: "Data not found" });
    }

    const updatedUser = await prisma.user.update({
      where: { phoneNumber },
      data: {
        hadir: attendance,
      },
    });

    return res.status(200).json({ success: true, updatedUser });
  } catch (error) {
    console.error("Failed to update sendAttendance:", error);
    return res
      .status(500)
      .json({ success: false, message: "Internal Server Error" });
  } finally {
    await prisma.$disconnect(); // Disconnect Prisma client
  }
}
