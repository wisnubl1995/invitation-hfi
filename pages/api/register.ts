import { PrismaClient } from "@prisma/client";

export default async function handler(req: any, res: any) {
  if (req.method !== "POST") {
    return res.status(405).json({ error: "Method not allowed" });
  }

  const prisma = new PrismaClient();

  try {
    const {
      fullName,
      companyName,
      phoneNumber,
      email,
      attendance,
      numberOfPax,
      isAllergic,
      allergicInfo,
    }: any = req.body;

    // Check if phoneNumber already exists
    const existingUser = await prisma.user.findUnique({
      where: {
        phoneNumber: phoneNumber,
      },
    });

    if (existingUser) {
      return res.status(400).json({ error: "Phone number already exists" });
    }

    // If phoneNumber does not exist, create new user
    const newUser = await prisma.user.create({
      data: {
        fullName,
        companyName,
        phoneNumber,
        email,
        registrationAttendance: attendance,
        numberOfPax,
        isAllergic,
        confirmAttendance: "null",
        seatNumber: "null",
        allergicInformations: {
          create: allergicInfo,
        },
      },
    });

    res
      .status(201)
      .json({ message: "User created successfully", user: newUser });
  } catch (error) {
    console.error("Error creating user:", error);
    res.status(500).json({ error: "Internal server error" });
  } finally {
    await prisma.$disconnect();
  }
}
