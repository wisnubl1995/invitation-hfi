import { useState } from "react";
import { PrismaClient } from "@prisma/client";
import Image from "next/image";
import hero from "@/public/img/hero.png";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Head from "next/head";
import logo from "@/public/img/logohfi.png";
import text from "@/public/img/TEXT.png";
import date from "@/public/img/date.png";
import date1 from "@/public/img/date1.png";
import date2 from "@/public/img/date2.png";
import date3 from "@/public/img/date3.png";
import date4 from "@/public/img/date4.png";
import location from "@/public/img/location.png";
import address from "@/public/img/address.png";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import Select from "react-select";

export default function Register() {
  const [fullName, setFullName] = useState("");
  const [companyName, setCompanyName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [email, setEmail] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const [isRegister, setIsRegister] = useState(false);
  const [attendance, setAttendance] = useState("");
  const [isContinue, setIsContinue] = useState(false);
  const [numberOfPax, setNumberOfPax] = useState("1 Pax");
  const [isAllergic, setIsAllergic] = useState("");

  const [allergicName, setAllergicName] = useState("");
  const [allergicDetail, setAllergicDetail] = useState("");

  const options = [
    { value: "1 Pax", label: "1 Pax" },
    { value: "2 Pax", label: "2 Pax" },
  ];

  const handleSubmit = async (e: any) => {
    setIsLoading(true);
    e.preventDefault();
    try {
      const response = await fetch("/api/register", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          fullName,
          companyName,
          phoneNumber,
          email,
          attendance,
          numberOfPax,
          allergicInfo: [
            {
              name: fullName,
              allergic: allergicDetail,
            },
          ],
          isAllergic,
        }),
      });
      if (response.ok) {
        console.log("User registered successfully!");
        toast.success("Registration successful!");
        const userData = {
          fullName,
          companyName,
          phoneNumber,
          email,
        };
        localStorage.setItem("userData", JSON.stringify(userData));
        setTimeout(() => {
          window.location.href = "/success";
        }, 900);
      } else {
        console.error("Error registering user:", response.statusText);
        toast.error("User Has Been Registered!");
      }
    } catch (error) {
      console.error("Error registering user:", error);
      toast.error("Error registering user!");
    } finally {
      setIsLoading(false);
    }
  };

  const handleContinue = async (e: any) => {
    e.preventDefault();
    if (attendance === "Yes") {
      setIsContinue(true);
    } else {
      setIsLoading(true);
      try {
        const response = await fetch("/api/register", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            fullName,
            companyName,
            phoneNumber,
            email,
            attendance,
          }),
        });
        if (response.ok) {
          console.log("User registered successfully!");
          toast.success("Registration successful!");
          setIsLoading(false);
          const userData = {
            fullName,
            companyName,
            phoneNumber,
            email,
            attendance,
          };
          localStorage.setItem("userData", JSON.stringify(userData));
          setTimeout(() => {
            window.location.href = "/thankyou";
          }, 900);
        } else {
          console.error("Error registering user:", response.statusText);
          toast.error("Error registering user!");
        }
      } catch (error) {
        console.error("Error registering user:", error);
        toast.error("Error registering user!");
      } finally {
        setIsLoading(false);
      }
    }
  };

  return (
    <div className="max-w-[480px] mx-auto flex flex-col h-[100vh] bg-main bg-no-repeat bg-cover bg-center">
      <Head>
        <title>Invitation HFI</title>
      </Head>
      <ToastContainer />
      <div className="w-full mx-auto text-white p-10">
        {isRegister ? (
          <>
            <div className="flex flex-col w-full items-center h-[80vh] lg:pt-0 pt-3 justify-center">
              <div className="w-full">
                {!isContinue ? (
                  <form
                    onSubmit={handleContinue}
                    className="text-[#ffffff] pt-10 "
                  >
                    <div className="mb-4">
                      <label
                        htmlFor="fullName"
                        className="block text-sm font-medium pl-3 text-[20px] text-center"
                      >
                        Full Name
                      </label>
                      <input
                        type="text"
                        id="fullName"
                        value={fullName}
                        onChange={(e) => setFullName(e.target.value)}
                        required
                        className="mt-1 p-2 block w-full rounded-full border-white border-[1px] bg-transparent"
                      />
                    </div>
                    <div className="mb-4">
                      <label
                        htmlFor="companyName"
                        className="block text-sm font-medium pl-3 text-[20px] text-center"
                      >
                        Company Name
                      </label>
                      <input
                        type="text"
                        id="companyName"
                        value={companyName}
                        onChange={(e) => setCompanyName(e.target.value)}
                        required
                        className="mt-1 p-2 block w-full rounded-full border-white border-[1px] bg-transparent"
                      />
                    </div>
                    <div className="mb-4">
                      <label
                        htmlFor="phoneNumber"
                        className="block text-sm font-medium pl-3 text-[20px] text-center"
                      >
                        Phone Number
                      </label>
                      <div className="flex mt-1 px-3 py-1 w-full rounded-full border-white border-[1px] bg-transparent  text-black">
                        <PhoneInput
                          country={"id"}
                          value={phoneNumber}
                          onChange={(phone) => setPhoneNumber(phone)}
                          enableSearch={true}
                          containerStyle={{ backgroundColor: "transparent" }}
                          buttonStyle={{
                            backgroundColor: "#0000",
                            opacity: "0.0 - 1.0;",
                            borderStyle: "none",
                          }}
                          inputStyle={{
                            backgroundColor: "#0000",
                            borderStyle: "none",
                            color: "white",
                          }}
                          dropdownStyle={{
                            borderRadius: "20px",
                          }}
                        />
                      </div>
                    </div>
                    <div className="mb-4">
                      <label
                        htmlFor="email"
                        className="block text-sm font-medium pl-3 text-[20px] text-center"
                      >
                        E-mail
                      </label>
                      <input
                        type="email"
                        id="email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        required
                        className="mt-1 p-2 block w-full rounded-full border-white border-[1px] bg-transparent"
                      />
                    </div>
                    <div className="mb-4">
                      <span className="block text-sm font-medium pl-3 text-[20px] text-center">
                        Attendance
                      </span>
                      <div className="mt-1 w-full  justify-center items-center flex pt-3 gap-16">
                        <label
                          htmlFor="attendanceYes"
                          className="inline-flex items-center mr-6"
                        >
                          <input
                            type="radio"
                            id="attendanceYes"
                            name="attendance"
                            value="Yes"
                            checked={attendance === "Yes"}
                            onChange={(e) => setAttendance(e.target.value)}
                            className="form-radio h-5 w-5 text-blue-600"
                          />
                          <span className="ml-2 text-sm">YES</span>
                        </label>
                        <label
                          htmlFor="attendanceNo"
                          className="inline-flex items-center"
                        >
                          <input
                            type="radio"
                            id="attendanceNo"
                            name="attendance"
                            value="No"
                            checked={attendance === "No"}
                            onChange={(e) => setAttendance(e.target.value)}
                            className="form-radio h-5 w-5 text-red-600"
                          />
                          <span className="ml-2 text-sm">NO</span>
                        </label>
                      </div>
                    </div>
                    <div className="flex justify-center pt-10">
                      <button
                        type="submit"
                        className=" font-sans px-6 py-1 bg-gradient-to-b from-[#D9D9D9] to-[#737373] border-white border-2 rounded-full w-10/12 text-[24px] font-bold tracking-wider hover:bg-gradient-to-b hover:from-[#737373] hover:to-[#D9D9D9] transition-all ease-out duration-200"
                        disabled={isLoading} // Disable button when loading
                      >
                        {isLoading ? "Loading..." : "CONTINUE"}{" "}
                        {/* Change button text when loading */}
                      </button>
                    </div>
                  </form>
                ) : (
                  <form
                    onSubmit={handleSubmit}
                    className="text-[#ffffff] pt-10 w-full"
                  >
                    <div className="mb-4">
                      <label
                        htmlFor="additionalInfo"
                        className="block text-sm font-medium pl-3 text-[20px] text-center"
                      >
                        Number Of Pax
                      </label>
                      <Select
                        id="additionalInfo"
                        placeholder="1 Pax"
                        value={"1 Pax"}
                        options={options}
                        onChange={(e: any) => {
                          setNumberOfPax(e.value);
                        }}
                        isDisabled
                        className="mt-1 py-2 px-5 block w-full text-center rounded-full  bg-transparent"
                        styles={{
                          control: (base) => ({
                            ...base,
                            backgroundColor: "transparent",
                            borderColor: "white",
                            borderRadius: "9999px",
                            padding: "0.5rem",
                          }),
                          menu: (base) => ({
                            ...base,
                            backgroundColor: "#737373",
                            width: "280px", // Atur lebar dropdown list
                          }),
                          option: (base) => ({
                            ...base,
                            color: "white",
                            width: "280px", // Atur lebar opsi
                          }),
                          singleValue: (base) => ({
                            ...base,
                            color: "white",
                          }),
                        }}
                      />
                    </div>
                    <div className="mb-4">
                      <span className="block text-sm font-medium pl-3 text-[20px] text-center">
                        Allergic Information
                      </span>
                      <div className="mt-1 w-full  justify-center items-center flex pt-3 gap-16">
                        <label
                          htmlFor="allergicYes"
                          className="inline-flex items-center mr-6"
                        >
                          <input
                            type="radio"
                            id="allergicYes"
                            name="allergic"
                            value="Yes"
                            checked={isAllergic === "Yes"}
                            onChange={(e) => setIsAllergic(e.target.value)}
                            className="form-radio h-5 w-5 text-blue-600"
                          />
                          <span className="ml-2 text-sm">YES</span>
                        </label>
                        <label
                          htmlFor="allergicNo"
                          className="inline-flex items-center"
                        >
                          <input
                            type="radio"
                            id="allergicNo"
                            name="allergic"
                            value="No"
                            checked={isAllergic === "No"}
                            onChange={(e) => setIsAllergic(e.target.value)}
                            className="form-radio h-5 w-5 text-red-600"
                          />
                          <span className="ml-2 text-sm">NO</span>
                        </label>
                      </div>
                    </div>
                    {isAllergic == "Yes" ? (
                      <>
                        <div className="mb-4 w-full ">
                          <table className="table-auto w-full border-spacing-0 rounded-xl border-white border-r-2 border-t-2 border-l-2 border-separate overflow-hidden">
                            <thead className="bg-[#454545] ">
                              <tr className="">
                                <td className="text-center border-b-2 border-r-2">
                                  Name
                                </td>
                                <td className="text-center border-b-2">
                                  Allergic Food
                                </td>
                              </tr>
                            </thead>
                            <tbody>
                              <tr className="w-full">
                                <td>
                                  <input
                                    type="text"
                                    placeholder="Name"
                                    value={fullName}
                                    onChange={(e) => {
                                      setAllergicName(e.target.value);
                                    }}
                                    className="w-full bg-transparent text-white px-5 py-2 border-b-2 border-r-2"
                                  />
                                </td>
                                <td>
                                  <input
                                    type="text"
                                    required
                                    placeholder="allergic"
                                    value={allergicDetail}
                                    onChange={(e) => {
                                      setAllergicDetail(e.target.value);
                                    }}
                                    className="w-full bg-transparent text-white px-5 py-2 border-b-2"
                                  />
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </>
                    ) : (
                      <></>
                    )}

                    <div className="flex justify-center pt-20">
                      <button
                        type="submit"
                        className=" font-sans px-6 py-1 bg-gradient-to-b from-[#D9D9D9] to-[#737373] border-white border-2 rounded-full w-10/12 text-[24px] font-bold tracking-wider hover:bg-gradient-to-b hover:from-[#737373] hover:to-[#D9D9D9] transition-all ease-out duration-200"
                        disabled={isLoading} // Disable button when loading
                      >
                        {isLoading ? "Loading..." : "SUBMIT"}{" "}
                        {/* Change button text when loading */}
                      </button>
                    </div>
                  </form>
                )}
              </div>
            </div>
          </>
        ) : (
          <>
            <div className="flex flex-col items-center justify-center h-[80vh] lg:mt-10 mt-5  gap-7">
              <div className="w-full flex flex-col items-center">
                <div className="w-12/12">
                  <Image className="" src={logo} alt="logo" />
                </div>
                <div className="lg:pt-1 pt-0">
                  <Image src={hero} alt="logo" />
                </div>
              </div>
              <div className="w-full flex flex-col items-center">
                <div className="w-11/12 border-b-[#D9D9D9] border-b-2">
                  <div className="w-full text-center justify-center flex">
                    <h1 className="text-[32px] uppercase font-bold bg-gradient-to-b from-[#FFFFFF] to-[#999999] inline-block text-transparent bg-clip-text">
                      Gala Dinner
                    </h1>
                  </div>
                </div>
                <div className="w-8/12 pt-3">
                  <Image src={date} alt="text" />
                </div>
              </div>
              <div className="w-full  flex flex-col items-center ">
                <div className="w-12/12 text-center">
                  <h1 className="lg:text-[30px] text-[24px] font-semibold">
                    Royal Glasshouse
                  </h1>
                  <h1 className="lg:text-[30px] text-[24px] -mt-3 font-semibold">
                    at Park Hyatt Hotel
                  </h1>
                </div>
                <div className="w-10/12 pt-2 text-center -mt-2">
                  <p className="lg:text-[16px] text-[12px] leading-tight tracking-wide">
                    MNC Tower, Jl Kebon Sirih 17-18, Menteng, Central Jakarta,
                    DKI Jakarta, Indonesia
                  </p>
                </div>
              </div>

              <div className="w-full font-sans flex flex-row">
                <div className="w-full font-sans  flex flex-col items-center ">
                  <div className="w-12/12">
                    <p className="lg:text-[24px] text-[16px]">
                      Start at:
                    </p>
                  </div>
                  <div className="bg-[#D9D9D9] h-[2px] lg:w-[120px] w-[100px] -mt-1 mb-1"></div>
                  <div className="w-12/12 -mt-1">
                    <p className="lg:text-[24px] text-[16px]">18:00 WIB</p>
                  </div>
                </div>
                <div className="w-full font-sans flex flex-col items-center">
                  <div className="w-12/12 ">
                    <p className="lg:text-[24px] text-[16px]">
                      Until:
                    </p>
                  </div>
                  <div className="bg-[#D9D9D9] h-[2px] w-[120px] -mt-1 mb-1"></div>
                  <div className="w-12/12 -mt-1">
                    <p className="lg:text-[24px] text-[16px]">21:30 WIB</p>
                  </div>
                </div>
              </div>
              <div className="w-full font-sans flex flex-col items-center lg:pt-8 pt-1">
                <button
                  onClick={() => {
                    setIsRegister(true);
                  }}
                  className="px-6 py-1 bg-gradient-to-b from-[#D9D9D9] to-[#737373] border-white border-2 rounded-full w-10/12 text-[24px] font-bold tracking-wider hover:bg-gradient-to-b hover:from-[#737373] hover:to-[#D9D9D9] transition-all ease-out duration-200"
                >
                  RESERVATION
                </button>
              </div>
            </div>
          </>
        )}
      </div>
    </div>
  );
}
