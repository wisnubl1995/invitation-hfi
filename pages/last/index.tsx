import { useEffect, useState } from "react";
import { PrismaClient } from "@prisma/client";
import Image from "next/image";
import hero from "@/public/img/hero.png";

import "react-toastify/dist/ReactToastify.css";
import logo from "@/public/img/logohfi.png";
import border from "@/public/img/Group.png";

import "react-phone-input-2/lib/style.css";
import { useRouter } from "next/router";
import Head from "next/head";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

type Props = {
  fullName: string;
  companyName: string;
  phoneNumber: string;
  email: string;
  seat: string;
};

export async function getServerSideProps(context: any) {
  const { query } = context;
  const { phoneNumber } = query;

  try {
    const response = await fetch(
      `https://invitation-hfi.vercel.app/api/fetch-single-user?phoneNumber=${encodeURIComponent(
        phoneNumber
      )}`
    );
    if (response.ok) {
      const user = await response.json();

      return {
        props: {
          initialUser: user,
        },
      };
    } else {
      return {
        props: { error: response.statusText },
      };
    }
  } catch (error: any) {
    return {
      props: { error: error.message },
    };
  }
}

const Index = ({ initialUser }: any) => {
  const [user, setUser] = useState(initialUser);

  const [attendance, setAttendance] = useState("");
  const [isSubmit, setIsSubmit] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const router = useRouter();
  const { fullName, companyName, phoneNumber, email, seat }: any = router.query;

  const spaceRegex = /-/g;

  const fullNameWithSpace = fullName?.replace(spaceRegex, " ");
  const companyNameWithSpace = companyName?.replace(spaceRegex, " ");
  const phoneNumberWithSpace = phoneNumber?.replace(spaceRegex, " ");
  const emailWithSpace = email?.replace(spaceRegex, " ");
  const seatWithSpace = seat?.replace(spaceRegex, " ");

  const sendAttendanceConfirmation = async (phoneNumber: any) => {
    try {
      const response = await fetch("/api/update-last-attendance", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          phoneNumber,
          attendance,
        }),
      });
      if (response.ok) {
        toast.success("Data Updated!");
        setIsSubmit(true);
      } else {
        toast.error("Error While Updating Data!");
      }
    } catch (error) {
      console.error("Gagal mengirim email:", error);
      toast.error("Error While Updating Data!");
    }
  };

  return (
    <div className="max-w-[480px] mx-auto flex flex-col h-[100vh] bg-main bg-no-repeat bg-cover">
      <Head>
        <title>User</title>
      </Head>
      <ToastContainer />

      <>
        <div className="flex flex-col items-center  gap-10 pt-28">
          <div className="w-full flex flex-col items-center">
            <div className="w-10/12 flex justify-center">
              <Image fetchPriority="auto" src={logo} alt="logo" />
            </div>
            <div className="pt-1 w-10/12">
              <Image src={hero} fetchPriority="auto" alt="logo" />
            </div>
          </div>
        </div>
        <div className="px-8 pt-6 pb-8 mb-4 flex flex-col items-center w-full">
          <div className="mb-4 w-full flex justify-center">
            <div className="bg-qrCode rounded-xl flex justify-center items-center w-[252px] h-[252px] relative">
              <Image
                className="absolute"
                src={border}
                alt="broder"
                data-fetchpriority="auto" // Menggunakan data-fetchpriority untuk properti HTML kustom
              />
              <Image
                className="w-[140px] h-[140px]"
                src={`${user.qrCode}`}
                alt="qrCode"
                width={160}
                height={160}
                data-fetchpriority="auto" // Menggunakan data-fetchpriority untuk properti HTML kustom
              />
            </div>
          </div>
          <div className="w-full mb-3 text-center justify-center flex">
            <h1 className="text-[32px]  font-bold bg-gradient-to-r from-[#CCB568] to-[#9A894F] inline-block text-transparent bg-clip-text">
              SEAT {seatWithSpace}
            </h1>
          </div>
          <div className="flex flex-col items-center justify-center text-white">
            <div className="mb-3 flex flex-col items-center ">
              <label className="block text-sm  mb-1">Full Name:</label>
              <div className="font-bold">{fullNameWithSpace}</div>
            </div>
            <div className="mb-3 flex flex-col items-center">
              <label className="block  text-sm d mb-1">Company Name:</label>
              <div className="font-bold">{companyNameWithSpace}</div>
            </div>
            <div className="mb-3 flex flex-col items-center">
              <label className="block  text-sm  mb-1">Phone Number:</label>
              <div className="font-bold">{phoneNumberWithSpace}</div>
            </div>
            <div className="mb-3 flex flex-col items-center">
              <label className="block text-sm  mb-1">Email:</label>
              <div className="font-bold">{emailWithSpace}</div>
            </div>
          </div>
        </div>
      </>
    </div>
  );
};

export default Index;
