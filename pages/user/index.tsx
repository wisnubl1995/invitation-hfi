import { useEffect, useState } from "react";
import { PrismaClient } from "@prisma/client";
import Image from "next/image";
import hero from "@/public/img/hero.png";

import "react-toastify/dist/ReactToastify.css";
import logo from "@/public/img/logohfi.png";
import border from "@/public/img/Group.png";

import "react-phone-input-2/lib/style.css";
import { useRouter } from "next/router";
import Head from "next/head";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

type Props = {
  fullName: string;
  companyName: string;
  phoneNumber: string;
  email: string;
  seat: string;
};

export async function getServerSideProps(context: any) {
  const { query } = context;
  const { phoneNumber } = query;

  try {
    const response = await fetch(
      `https://rsvp-hfi10th-anniversary.vercel.app/api/fetch-single-user?phoneNumber=${encodeURIComponent(
        phoneNumber
      )}`
    );
    if (response.ok) {
      const user = await response.json();

      return {
        props: {
          initialUser: user,
        },
      };
    } else {
      return {
        props: { error: response.statusText },
      };
    }
  } catch (error: any) {
    return {
      props: { error: error.message },
    };
  }
}

const Index = ({ initialUser }: any) => {
  const [user, setUser] = useState(initialUser);

  console.log(user);

  const [attendance, setAttendance] = useState("");
  const [isSubmit, setIsSubmit] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isAllowed, setIsAllowed] = useState(true);

  const router = useRouter();

  const spaceRegex = /-/g;

  const sendAttendanceConfirmation = async (phoneNumber: any) => {
    try {
      const response = await fetch("/api/update-last-attendance", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          phoneNumber,
          attendance,
        }),
      });
      if (response.ok) {
        toast.success("Data Updated!");
        setIsSubmit(true);
      } else {
        toast.error("Error While Updating Data!");
      }
    } catch (error) {
      console.error("Gagal mengirim email:", error);
      toast.error("Error While Updating Data!");
    }
  };

  useEffect(() => {
    const checkDate = () => {
      const currentDate = new Date();
      const targetDate = new Date("2024-07-11");

      if (currentDate < targetDate) {
        setIsAllowed(false);
      }
    };

    console.log("Component mounted");

    checkDate();

    return () => {
      console.log("Component unmounted");
    };
  }, []);

  if (!isAllowed) {
    return (
      <div className="flex items-center justify-center h-screen">
        <h1 className="text-4xl font-bold">You Are Not Allowed!</h1>
      </div>
    );
  }

  return (
    <div className="max-w-[480px] mx-auto flex flex-col h-[100vh] bg-main bg-no-repeat bg-cover">
      <Head>
        <title>User</title>
      </Head>
      <ToastContainer />
      {isSubmit == true || user.hadir == "Yes" ? (
        <>
          <div className="flex flex-col items-center  gap-10 pt-28">
            <div className="w-full flex flex-col items-center">
              <div className="w-10/12 flex justify-center">
                <Image fetchPriority="auto" src={logo} alt="logo" />
              </div>
              <div className="pt-1 w-10/12">
                <Image src={hero} fetchPriority="auto" alt="logo" />
              </div>
            </div>
          </div>
          <div className="px-8 pt-6 pb-8 mb-4 flex flex-col items-center w-full">
            <div className="mb-4 w-full flex justify-center"></div>
            <div className="w-full mb-3 text-center justify-center flex">
              <h1 className="text-[32px]  font-bold bg-gradient-to-r from-[#CCB568] to-[#9A894F] inline-block text-transparent bg-clip-text">
                {user.seatNumber}
              </h1>
            </div>
            <div className="flex flex-col items-center justify-center text-white">
              <div className="mb-3 flex flex-col items-center ">
                <label className="block text-sm  mb-1">Full Name:</label>
                <div className="font-bold">{user.fullName}</div>
              </div>
              <div className="mb-3 flex flex-col items-center">
                <label className="block  text-sm d mb-1">Company Name:</label>
                <div className="font-bold">{user.companyName}</div>
              </div>
              <div className="mb-3 flex flex-col items-center">
                <h1>USER</h1>
                <h1>HAS</h1>
                <h1>ATTENDED THE EVENT</h1>
              </div>
            </div>
          </div>
        </>
      ) : (
        <>
          <div className="flex flex-col items-center  gap-5 pt-24">
            <div className="w-full flex flex-col items-center">
              <div className="w-10/12 flex justify-center">
                <Image fetchPriority="auto" src={logo} alt="logo" />
              </div>
              <div className="pt-1 w-10/12">
                <Image src={hero} fetchPriority="auto" alt="logo" />
              </div>
            </div>
          </div>
          <div className="px-8 pt-3 pb-8 mb-4 flex flex-col items-center w-full">
            <div className="w-full mb-3 text-center justify-center flex">
              <h1 className="text-[32px]  font-bold bg-gradient-to-r from-[#CCB568] to-[#9A894F] inline-block text-transparent bg-clip-text">
                {user.seatNumber ? user.seatNumber : "[Belum Di Isi]"}
              </h1>
            </div>
            <div className="flex flex-col items-center justify-center text-white">
              <div className="mb-3 flex flex-col items-center ">
                <label className="block text-sm  mb-1">Full Name:</label>
                <div className="font-bold">{user.fullName}</div>
              </div>
              <div className="mb-3 flex flex-col items-center">
                <label className="block  text-sm d mb-1">Company Name:</label>
                <div className="font-bold">{user.companyName}</div>
              </div>
              {user.newData != true ? <></> : <div></div>}

              <div className="pt-3">
                <span className="block text-sm font-medium pl-3 text-[20px] text-center">
                  Attendance
                </span>
                <div className="mt-1 w-full  justify-center items-center flex pt-3 gap-16">
                  <label
                    htmlFor="attendanceYes"
                    className="inline-flex items-center mr-6"
                  >
                    <input
                      type="radio"
                      id="attendanceYes"
                      name="attendance"
                      value="Yes"
                      checked={attendance === "Yes"}
                      onChange={(e) => setAttendance(e.target.value)}
                      className="form-radio h-5 w-5 text-blue-600"
                    />
                    <span className="ml-2 text-sm">YES</span>
                  </label>
                  <label
                    htmlFor="attendanceNo"
                    className="inline-flex items-center"
                  >
                    <input
                      type="radio"
                      id="attendanceNo"
                      name="attendance"
                      value="No"
                      checked={attendance === "No"}
                      onChange={(e) => setAttendance(e.target.value)}
                      className="form-radio h-5 w-5 text-red-600"
                    />
                    <span className="ml-2 text-sm">NO</span>
                  </label>
                </div>
              </div>

              <div className="flex justify-center pt-10">
                <button
                  type="submit"
                  onClick={() => {
                    sendAttendanceConfirmation(user.phoneNumber);
                  }}
                  className=" font-sans px-6 py-1 text-center w bg-gradient-to-b from-[#D9D9D9] to-[#737373] border-white border-2 rounded-full w-full text-[24px] font-bold tracking-wider hover:bg-gradient-to-b hover:from-[#737373] hover:to-[#D9D9D9] transition-all ease-out duration-200"
                  disabled={isLoading} // Disable button when loading
                >
                  {isLoading ? "Loading..." : "SUBMIT"}{" "}
                  {/* Change button text when loading */}
                </button>
              </div>
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default Index;
