import Navbar from "@/components/Navbar";
import { PrismaClient } from "@prisma/client";
import Head from "next/head";
import { FormEvent, useState, useMemo } from "react";
import { ToastContainer, toast } from "react-toastify";
import Modal from "react-modal";
import Select from "react-select";
import "react-toastify/dist/ReactToastify.css";
import * as XLSX from "xlsx";

const prisma = new PrismaClient();

export async function getServerSideProps() {
  const users = await prisma.user.findMany();

  return {
    props: {
      initialUsers: users,
    },
  };
}

const IndexPage = ({ initialUsers }: any) => {
  const [users, setUsers] = useState(initialUsers);
  const [loadingItemId, setLoadingItemId] = useState(null);
  const [activeTab, setActiveTab] = useState("hadir");
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [searchTerm, setSearchTerm] = useState("");
  const [selectedCompany, setSelectedCompany] = useState<any>(null);
  const [selectedUser, setSelectedUser] = useState<any>(null);
  const [attendance, setAttendance] = useState("");
  const [isSubmit, setIsSubmit] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [searchMode, setSearchMode] = useState("company");

  const fetchUsers = async () => {
    try {
      const response = await fetch("/api/fetch-users");
      if (response.ok) {
        const data = await response.json();
        setUsers(data);
      } else {
        console.error("Failed to fetch users:", response.statusText);
      }
    } catch (error) {
      console.error("Failed to fetch users:", error);
    }
  };

  const filteredUsers =
    activeTab === "hadir"
      ? users.filter((user: any) => user.hadir == "Yes")
      : users.filter(
          (user: any) => user.hadir != "Yes" && user.seatNumber != "null"
        );

  const sortedUsers = filteredUsers.sort((a: any, b: any) =>
    a.companyName.localeCompare(b.companyName)
  );

  const attendanceUser = useMemo(
    () =>
      users
        .filter((user: any) => user.hadir != "Yes" && user.seatNumber != "null")
        .sort((a: any, b: any) => a.fullName.localeCompare(b.fullName)),
    [users]
  );

  const options = useMemo(
    () =>
      attendanceUser.map((user: any) => ({
        value: user.id,
        label: user.fullName,
        companyName: user.companyName,
        phoneNumber: user.phoneNumber,
      })),
    [attendanceUser]
  );

  const companyOptions = useMemo(
    () =>
      Array.from(
        new Set(
          users
            .filter((user: any) => user.seatNumber != "null")
            .map((user: any) => user.companyName)
        )
      )
        .sort((a: any, b: any) => a.localeCompare(b)) // Sort companies alphabetically
        .map((companyName) => ({ value: companyName, label: companyName })),
    [users]
  );

  const userOptions = useMemo(
    () =>
      selectedCompany
        ? users
            .filter((user: any) => user.companyName === selectedCompany.value)
            .map((user: any) => ({
              value: user.id,
              label: user.fullName,
              companyName: user.companyName,
              phoneNumber: user.phoneNumber,
            }))
        : [],
    [selectedCompany, users]
  );

  const handleCompanyChange = (selectedOption: any) => {
    setSelectedCompany(selectedOption);
    setSelectedUser(null);
  };

  const handleUserChange = (selectedOption: any) => {
    setSelectedUser(selectedOption);
  };

  const sendAttendanceConfirmation = async (phoneNumber: any) => {
    try {
      const response = await fetch("/api/update-last-attendance", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          phoneNumber,
          attendance,
        }),
      });
      if (response.ok) {
        toast.success("Data Updated!");
        setIsSubmit(true);
      } else {
        toast.error("Error While Updating Data!");
      }
    } catch (error) {
      console.error("Gagal mengirim email:", error);
      toast.error("Error While Updating Data!");
    }
  };

  const closeModalAndReload = () => {
    setTimeout(() => {
      setIsModalOpen(false);
      fetchUsers();
    }, 2000); // 2 seconds delay
  };

  const exportToExcel = () => {
    const worksheet = XLSX.utils.json_to_sheet(
      sortedUsers.map((user: any) => ({
        Name: user.fullName,
        "Company Name": user.companyName,
        Email: user.email,
        "Phone Number": user.phoneNumber,
        Seat: user.seatNumber,
        Link: `https://rsvp-hfi10th-anniversary.vercel.app/?id=${user.phoneNumber}`,
      }))
    );
    const workbook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workbook, worksheet, "Users");
    XLSX.writeFile(workbook, "UsersData-AttendaceFinal.xlsx");
  };

  const totalParticipants = useMemo(
    () => users.filter((user: any) => user.seatNumber != "null").length,
    [users]
  );

  const attendeesCount = useMemo(
    () =>
      users.filter(
        (user: any) => user.hadir === "Yes" && user.seatNumber != "null"
      ).length,
    [users]
  );

  const attendancePercentage = totalParticipants
    ? (attendeesCount / totalParticipants) * 100
    : 0;

  return (
    <div className="flex">
      <Navbar />
      <div className="container mx-auto p-6">
        <Head>
          <title>User Event Attendance (Final)</title>
        </Head>
        <ToastContainer />
        <h1 className="text-3xl font-bold mb-4">
          User Event Attendance (Final)
        </h1>

        {activeTab == "hadir" ? (
          <>
            <div className="mb-4">
              <p className="text-xl">
                Attendees: {attendeesCount} / {totalParticipants} (
                {attendancePercentage.toFixed(2)} %)
              </p>
            </div>
          </>
        ) : (
          <></>
        )}

        <div className="mb-4 flex justify-between">
          <div>
            <button
              className={`mr-2 p-2 rounded-md focus:outline-none ${
                activeTab === "hadir"
                  ? "bg-indigo-600 text-white"
                  : "bg-gray-200 text-gray-600"
              }`}
              onClick={() => setActiveTab("hadir")}
            >
              Daftar Hadir
            </button>
            <button
              className={`p-2 rounded-md focus:outline-none ${
                activeTab === "tidak-hadir"
                  ? "bg-indigo-600 text-white"
                  : "bg-gray-200 text-gray-600"
              }`}
              onClick={() => setActiveTab("tidak-hadir")}
            >
              Daftar Tidak Hadir
            </button>
          </div>
          <div>
            <button
              className="p-2 rounded-md bg-blue-500 text-white mr-2"
              onClick={() => setIsModalOpen(true)}
            >
              Submit Daftar Hadir Manual
            </button>
            <button
              onClick={exportToExcel}
              className="p-2 rounded-md bg-green-500 text-white"
            >
              Export to Excel
            </button>
          </div>
        </div>

        <Modal
          ariaHideApp={false}
          isOpen={isModalOpen}
          onRequestClose={() => setIsModalOpen(false)}
          contentLabel="Example Modal"
          className="fixed inset-0 flex items-center justify-center bg-gray-800 bg-opacity-50"
        >
          <div className="bg-white w-full md:w-6/12 md:px-5 lg:w-6/12 px-2 lg:px-24 py-16 rounded-md relative">
            <div className="flex justify-center mb-4">
              <button
                className={`mr-2 p-2 rounded-md ${
                  searchMode === "company"
                    ? "bg-indigo-600 text-white"
                    : "bg-gray-200 text-gray-600"
                }`}
                onClick={() => {
                  setSearchMode("company");
                  setSelectedUser(null);
                  setSelectedCompany(null);
                }}
              >
                Search by Company
              </button>
              <button
                className={`p-2 rounded-md ${
                  searchMode === "name"
                    ? "bg-indigo-600 text-white"
                    : "bg-gray-200 text-gray-600"
                }`}
                onClick={() => {
                  setSearchMode("name");
                  setSelectedUser(null);
                  setSelectedCompany(null);
                }}
              >
                Search by Name
              </button>
            </div>

            {searchMode == "company" ? (
              <>
                <h2 className="text-2xl font-bold mb-4">
                  Search Attendance Company
                </h2>
                <Select
                  options={companyOptions}
                  onChange={handleCompanyChange}
                  placeholder="Search by company name"
                  className="mb-4"
                />
                <Select
                  options={userOptions}
                  onChange={handleUserChange}
                  placeholder="Search by name"
                  className="mb-4"
                  isDisabled={!selectedCompany}
                />
              </>
            ) : (
              <>
                <h2 className="text-2xl font-bold mb-4">
                  Search Attendance Name
                </h2>
                <Select
                  options={options}
                  onChange={handleUserChange}
                  placeholder="Search by name"
                  className="mb-4"
                />
                <input
                  type="text"
                  value={selectedUser ? selectedUser.companyName : ""}
                  disabled
                  className="mb-4 p-2 border rounded-md w-full"
                />
              </>
            )}

            <div className="pt-3">
              <span className="block text-sm font-medium pl-3 text-[20px] text-center">
                Attendance
              </span>
              <div className="mt-1 w-full justify-center items-center flex pt-3 gap-16">
                <label
                  htmlFor="attendanceYes"
                  className="inline-flex items-center mr-6"
                >
                  <input
                    type="radio"
                    id="attendanceYes"
                    name="attendance"
                    value="Yes"
                    checked={attendance === "Yes"}
                    onChange={(e) => setAttendance(e.target.value)}
                    className="form-radio h-5 w-5 text-blue-600"
                  />
                  <span className="ml-2 text-sm">YES</span>
                </label>
                <label
                  htmlFor="attendanceNo"
                  className="inline-flex items-center"
                >
                  <input
                    type="radio"
                    id="attendanceNo"
                    name="attendance"
                    value="No"
                    checked={attendance === "No"}
                    onChange={(e) => setAttendance(e.target.value)}
                    className="form-radio h-5 w-5 text-red-600"
                  />
                  <span className="ml-2 text-sm">NO</span>
                </label>
              </div>
            </div>
            <div className="flex justify-center pt-10">
              <button
                type="submit"
                onClick={() => {
                  sendAttendanceConfirmation(selectedUser.phoneNumber);
                  closeModalAndReload();
                }}
                className="font-sans px-6 py-1 text-center w bg-blue-500 border-white border-2 rounded-full w-full text-[24px] font-bold tracking-wider hover:bg-gradient-to-b hover:from-[#737373] hover:to-[#D9D9D9] transition-all ease-out duration-200"
              >
                SUBMIT
              </button>
            </div>
            <button
              className="mt-4 p-2 rounded-md bg-red-500 text-white absolute top-0 right-5"
              onClick={() => setIsModalOpen(false)}
            >
              X
            </button>
          </div>
        </Modal>
        <div className="w-full overflow-x-scroll">
          <table className="w-full border-collapse">
            <thead>
              <tr>
                <th className="p-3 font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                  Name
                </th>
                <th className="p-3 font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                  Company Name
                </th>
                <th className="p-3 font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                  Email
                </th>
                <th className="p-3 font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                  Phone Number
                </th>
                <th className="p-3 font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                  Seat Number
                </th>
              </tr>
            </thead>
            <tbody>
              {sortedUsers.map((user: any, i: any) => (
                <tr key={i} className="bg-gray-50">
                  <td className="p-3 border border-gray-300">
                    {user.fullName}
                  </td>
                  <td className="p-3 border border-gray-300">
                    {user.companyName}
                  </td>
                  <td className="p-3 border border-gray-300">{user.email}</td>
                  <td className="p-3 border border-gray-300">
                    {user.phoneNumber}
                  </td>
                  <td className="p-3 border border-gray-300">
                    {user.seatNumber}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default IndexPage;
