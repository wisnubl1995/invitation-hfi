import Navbar from "@/components/Navbar";
import { PrismaClient } from "@prisma/client";
import Head from "next/head";
import { FormEvent, useEffect, useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import * as XLSX from "xlsx";
import { saveAs } from "file-saver";

const prisma = new PrismaClient();

export async function getServerSideProps() {
  const users = await prisma.user.findMany();

  return {
    props: {
      initialUsers: users,
    },
  };
}

const IndexPage = ({ initialUsers }: any) => {
  const [users, setUsers] = useState(initialUsers);
  const [loadingItemId, setLoadingItemId] = useState(null);
  const [activeTab, setActiveTab] = useState("tidak-hadir");
  const [fullName, setFullName] = useState("");
  const [companyName, setCompanyName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [email, setEmail] = useState("");
  const [seatNumbers, setSeatNumbers] = useState(Array(users.length).fill(""));

  const handleSeatNumberChange = (index: any, value: any) => {
    const newSeatNumbers = [...seatNumbers];
    newSeatNumbers[index] = value;
    setSeatNumbers(newSeatNumbers);
  };

  const fetchUsers = async () => {
    try {
      const response = await fetch("/api/fetch-users");
      if (response.ok) {
        const data = await response.json();
        setUsers(data);
      } else {
        console.error("Failed to fetch users:", response.statusText);
      }
    } catch (error) {
      console.error("Failed to fetch users:", error);
    }
  };

  const sendEmailConfirmation = async (
    phoneNumber: any,
    itemId: any,
    fullName: any,
    companyName: any,
    email: any
  ) => {
    const seatNumber = seatNumbers[itemId];
    try {
      setLoadingItemId(itemId);
      const response = await fetch("/api/update-seating", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          phoneNumber,
          seatNumber,
          fullName,
          companyName,
          email,
          protocolAndDomain,
        }),
      });
      if (response.ok) {
        toast.success("Registration successful!");
        await fetchUsers();
        const newSeatNumbers = [...seatNumbers];
        newSeatNumbers[itemId] = "";
        setSeatNumbers(newSeatNumbers);
      } else {
        toast.error("Error registering user!");
      }
    } catch (error) {
      console.error("Gagal mengirim email:", error);
      toast.error("Error registering user!");
    } finally {
      setLoadingItemId(null);
    }
  };
  const fetchAllergicDetails = async (userId: any) => {
    try {
      const response = await fetch(`/api/allergic-info?userId=${userId}`);
      if (response.ok) {
        const data = await response.json();
        return data;
      } else {
        console.error("Failed to fetch allergic info:", response.statusText);
        return [];
      }
    } catch (error) {
      console.error("Failed to fetch allergic info:", error);
      return [];
    }
  };

  const exportToExcel = async () => {
    const data = await Promise.all(
      filteredUsers.map(async (user: any, index: any) => {
        const allergicDetails =
          user.isAllergic === "Yes" ? await fetchAllergicDetails(user.id) : [];
        const allergicInfo = allergicDetails
          .map(
            (detail: any, idx: any) =>
              `[${idx + 1}] ${detail.name}: ${detail.allergic}  `
          )
          .join("\n");

        return {
          ID: user.phoneNumber,
          Nama: user.fullName,
          "Company Name": user.companyName,
          Email: user.newData ? "no data" : user.email,
          "Phone Number": user.newData ? "no data" : user.phoneNumber,
          "List Allergic": allergicInfo || "No Allergic",
          Seat: user.seatNumber,
          Link: `https://rsvp-hfi10th-anniversary.vercel.app/?id=${user.phoneNumber}`,
        };
      })
    );

    // Urutkan data berdasarkan companyName
    const sortedData = data.sort((a, b) =>
      a["Company Name"].localeCompare(b["Company Name"])
    );

    // Buat worksheet dan workbook
    const worksheet = XLSX.utils.json_to_sheet(sortedData);
    const workbook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workbook, worksheet, "Users");

    // Konversi workbook menjadi buffer dan buat blob
    const excelBuffer = XLSX.write(workbook, {
      bookType: "xlsx",
      type: "array",
    });

    // Buat nama file dengan timestamp
    const now = new Date();
    const datetime = now.toISOString().slice(0, 19).replace(/:/g, "-");

    const dataBlob = new Blob([excelBuffer], {
      type: "application/octet-stream",
    });

    saveAs(dataBlob, `user_registration-${datetime}.xlsx`);
  };

  const filteredUsers =
    activeTab === "hadir"
      ? users.filter((user: any) => user.seatNumber == "null")
      : users.filter((user: any) => user.seatNumber != "null");

  const [protocolAndDomain, setProtocolAndDomain] = useState("");

  const getProtocolAndDomain = () => {
    const protocolAndDomain =
      window.location.protocol + "//" + window.location.host;
    setProtocolAndDomain(protocolAndDomain);
  };

  useEffect(() => {
    getProtocolAndDomain();
  }, []);

  return (
    <div className="flex">
      <Navbar />
      <div className="container w-10/12 p-6">
        <Head>
          <title>Manage User Seating</title>
        </Head>
        <ToastContainer />
        <h1 className="text-3xl font-bold mb-4">User Seating</h1>

        <div className="mb-4">
          <button
            className={`p-2 rounded-md focus:outline-none ${
              activeTab === "tidak-hadir"
                ? "bg-indigo-600 text-white"
                : "bg-gray-200 text-gray-600"
            }`}
            onClick={() => setActiveTab("tidak-hadir")}
          >
            User Data
          </button>
          <button
            disabled
            className={`mr-2 p-2 rounded-md focus:outline-none cursor-not-allowed ${
              activeTab === "hadir"
                ? "bg-indigo-600 text-white"
                : "bg-gray-200 text-gray-600"
            }`}
            onClick={() => setActiveTab("hadir")}
          >
            Data DUMP
          </button>
          <button
            className="p-2 rounded-md bg-green-500 text-white"
            onClick={exportToExcel}
          >
            Export to Excel
          </button>
        </div>
        <div className="overflow-x-auto shadow-2xl">
          <table className="w-full max-w-none border-collapse table-fixed min-w-[1366px]">
            <thead>
              <tr>
                <th className="p-3 w-[50px] font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                  No
                </th>
                <th className="p-3 w-[200px] font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                  Name
                </th>
                <th className="p-3 w-[280px] font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                  Company Name
                </th>
                <th className="p-3 w-[280px] font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                  ID's
                </th>
                <th className="p-3 w-[200px] font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                  Seat
                </th>
              </tr>
            </thead>
            <tbody>
              {filteredUsers
                .filter((user: any) => user.registrationAttendance == "Yes")
                .map((user: any, index: any) => (
                  <tr key={index} className="bg-gray-50">
                    <td className="p-3 border border-gray-300">{index + 1}</td>
                    <td className="p-3 border border-gray-300">
                      {user.fullName}
                    </td>
                    <td className="p-3 border border-gray-300">
                      {user.companyName}
                    </td>
                    <td className="p-3 border border-gray-300">
                      {user.phoneNumber}
                    </td>
                    <td className="p-3 border border-gray-300 flex justify-center items-center gap-2 ">
                      {user.seatNumber == "null" ? (
                        <>
                          <input
                            type="text"
                            value={seatNumbers[index]}
                            onChange={(e) =>
                              handleSeatNumberChange(index, e.target.value)
                            }
                            required
                            className="mt-1 p-2 block w-6/12 rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                          />
                          <button
                            type="submit"
                            onClick={() =>
                              sendEmailConfirmation(
                                user.phoneNumber,
                                index,
                                user.fullName,
                                user.companyName,
                                user.email
                              )
                            }
                            className={`transition-all ease-out duration-200 py-2 px-4 border border-transparent rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 ${
                              loadingItemId === index
                                ? "opacity-50 cursor-not-allowed"
                                : ""
                            }`}
                            disabled={loadingItemId === index}
                          >
                            {loadingItemId === index ? "Loading..." : "Confirm"}
                          </button>
                        </>
                      ) : (
                        <>
                          <input
                            type="text"
                            disabled
                            value={user.seatNumber}
                            onChange={(e) =>
                              handleSeatNumberChange(index, e.target.value)
                            }
                            className="mt-1 p-2 block w-6/12 rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                          />
                        </>
                      )}
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default IndexPage;
