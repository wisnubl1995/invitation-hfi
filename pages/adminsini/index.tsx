import { useRouter } from "next/router";
import { useEffect } from "react";

const Adminsini = () => {
  const router = useRouter();

  useEffect(() => {
    router.push("/adminsini/registration");
  }, []);

  return null; // Return null or any loading indicator while redirecting
};

export default Adminsini;
