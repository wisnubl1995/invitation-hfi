import Navbar from "@/components/Navbar";
import { PrismaClient } from "@prisma/client";
import Head from "next/head";
import { FormEvent, useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const prisma = new PrismaClient();

export async function getServerSideProps() {
  const users = await prisma.user.findMany();

  return {
    props: {
      initialUsers: users,
    },
  };
}

const IndexPage = ({ initialUsers }: any) => {
  const [users, setUsers] = useState(initialUsers);
  const [loadingItemId, setLoadingItemId] = useState(null);
  const [activeTab, setActiveTab] = useState("hadir");
  const [fullName, setFullName] = useState("");
  const [companyName, setCompanyName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [email, setEmail] = useState("");
  const [seatNumbers, setSeatNumbers] = useState(Array(users.length).fill(""));

  const handleSeatNumberChange = (index: any, value: any) => {
    const newSeatNumbers = [...seatNumbers];
    newSeatNumbers[index] = value;
    setSeatNumbers(newSeatNumbers);
  };

  const fetchUsers = async () => {
    try {
      const response = await fetch("/api/fetch-users");
      if (response.ok) {
        const data = await response.json();
        setUsers(data);
      } else {
        console.error("Failed to fetch users:", response.statusText);
      }
    } catch (error) {
      console.error("Failed to fetch users:", error);
    }
  };

  const sendEmailConfirmation = async (
    phoneNumber: any,
    itemId: any,
    fullName: any,
    companyName: any,
    email: any,
    qrCode: any,
    url: any
  ) => {
    const seatNumber = seatNumbers[itemId];
    try {
      setLoadingItemId(itemId);
      const response = await fetch("/api/seating", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          phoneNumber,
          seatNumber,
          fullName,
          companyName,
          email,
          qrCode,
          url,
        }),
      });
      if (response.ok) {
        toast.success("RSend Email successful!");
        await fetchUsers();
        const newSeatNumbers = [...seatNumbers];
        newSeatNumbers[itemId] = "";
        setSeatNumbers(newSeatNumbers);
      } else {
        toast.error("Error Send Email user!");
      }
    } catch (error) {
      console.error("Gagal mengirim email:", error);
      toast.error("Error registering user!");
    } finally {
      setLoadingItemId(null);
    }
  };

  return (
    <div className="flex">
      <Navbar />
      <div className="container mx-auto p-6">
        <Head>
          <title>Manage User Seating</title>
        </Head>
        <ToastContainer />
        <h1 className="text-3xl font-bold mb-4">User Seating</h1>

        <table className="w-full border-collapse">
          <thead>
            <tr>
              <th className="p-3 font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                Name
              </th>
              <th className="p-3 font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                Company Name
              </th>
              <th className="p-3 font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                Email
              </th>
              <th className="p-3 font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                Phone Number
              </th>
              <th className="p-3 font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                Seat
              </th>
              <th className="p-3 font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                QR Code
              </th>
            </tr>
          </thead>
          <tbody>
            {users
              .filter((user: any) => user.seatNumber != "null")
              .map((user: any, index: any) => (
                <tr key={index} className="bg-gray-50">
                  <td className="p-3 border border-gray-300">
                    {user.fullName}
                  </td>
                  <td className="p-3 border border-gray-300">
                    {user.companyName}
                  </td>
                  <td className="p-3 border border-gray-300">{user.email}</td>
                  <td className="p-3 border border-gray-300">
                    {user.phoneNumber}
                  </td>
                  <td className="p-3 border border-gray-300">
                    {user.seatNumber}
                  </td>
                  <td className="p-3 border border-gray-300 flex justify-center items-center gap-2 ">
                    <img className="w-4/12" src={user.qrConfirm} />
                  </td>
                  <td className="p-3 border border-gray-300 flex justify-center items-center gap-2">
                    <button
                      type="submit"
                      onClick={() =>
                        sendEmailConfirmation(
                          user.phoneNumber,
                          index,
                          user.fullName,
                          user.companyName,
                          user.email,
                          user.qrConfirm,
                          user.url
                        )
                      }
                      className={`transition-all ease-out duration-200  py-2 px-4 border border-transparent rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 ${
                        loadingItemId === index
                          ? "opacity-50 cursor-not-allowed"
                          : ""
                      }`} // Disable button and change cursor when loading
                      disabled={loadingItemId === index} // Disable button when loading
                    >
                      {loadingItemId === index
                        ? "Loading..."
                        : "Send QR to Email"}{" "}
                      {/* Change button text when loading */}
                    </button>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default IndexPage;
