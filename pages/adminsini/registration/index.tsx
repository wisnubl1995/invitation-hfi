import Navbar from "@/components/Navbar";
import { PrismaClient } from "@prisma/client";
import Head from "next/head";
import { useEffect, useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import * as XLSX from "xlsx";
import { saveAs } from "file-saver";
import Link from "next/link";
import Image from "next/image";

const prisma = new PrismaClient();

export async function getServerSideProps() {
  const users = await prisma.user.findMany();

  return {
    props: {
      initialUsers: users,
    },
  };
}

const IndexPage = ({ initialUsers }: any) => {
  const [users, setUsers] = useState(initialUsers);
  const [isLoading, setIsLoading] = useState(false);
  const [loadingItemId, setLoadingItemId] = useState(null);
  const [activeTab, setActiveTab] = useState("hadir");
  const [allergicPopUp, setIsAllergicPopUp] = useState(false);
  const [allergicInfo, setAllergicInfo] = useState([]);
  const [selectedUser, setSelectedUser] = useState<any>(null);
  const [currentPage, setCurrentPage] = useState(1); // State untuk halaman saat ini
  const pageSize = 10; // Jumlah data per halaman

  const fetchAllergicInfo = async (userId: any) => {
    try {
      const response = await fetch(`/api/allergic-info?userId=${userId}`);
      if (response.ok) {
        const data = await response.json();
        setAllergicInfo(data);
        setIsAllergicPopUp(true);
      } else {
        console.error("Failed to fetch allergic info:", response.statusText);
      }
    } catch (error) {
      console.error("Failed to fetch allergic info:", error);
    }
  };

  const fetchAllergicDetails = async (userId: any) => {
    try {
      const response = await fetch(`/api/allergic-info?userId=${userId}`);
      if (response.ok) {
        const data = await response.json();
        return data;
      } else {
        console.error("Failed to fetch allergic info:", response.statusText);
        return [];
      }
    } catch (error) {
      console.error("Failed to fetch allergic info:", error);
      return [];
    }
  };

  const fetchUsers = async () => {
    try {
      const response = await fetch("/api/fetch-users");
      if (response.ok) {
        const data = await response.json();
        setUsers(data);
      } else {
        console.error("Failed to fetch users:", response.statusText);
      }
    } catch (error) {
      console.error("Failed to fetch users:", error);
    }
  };

  const sendEmailConfirmation = async (
    email: any,
    phoneNumber: any,
    fullName: any,
    companyName: any,
    itemId: any
  ) => {
    try {
      setLoadingItemId(itemId);
      const response = await fetch("/api/contact", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email, phoneNumber, fullName, companyName }),
      });
      if (response.ok) {
        toast.success("Email has been sent!!");
        await updateSendAttendance(phoneNumber);
        await fetchUsers();
      } else {
        toast.error("Error sending message!");
      }
    } catch (error) {
      console.error("Gagal mengirim email:", error);
      toast.error("Error sending message!");
    } finally {
      setLoadingItemId(null);
    }
  };

  const updateSendAttendance = async (phoneNumber: any) => {
    try {
      const response = await fetch("/api/update-send-attendance", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ phoneNumber }),
      });
      if (!response.ok) {
        console.error("Gagal memperbarui data sendAttendance!");
      }
    } catch (error) {
      console.error("Gagal memperbarui data sendAttendance:", error);
    }
  };

  const generateQr = async (phoneNumber: any, itemId: any) => {
    try {
      setIsLoading(true);
      const response = await fetch("/api/update-barcode", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          phoneNumber,
          protocolAndDomain,
        }),
      });
      if (response.ok) {
        toast.success("Generating QR successful!");
        setIsLoading(false);
        await fetchUsers();
      } else {
        setIsLoading(false);
        toast.error("Error Generating user!");
      }
    } catch (error) {
      setIsLoading(false);
      console.error("Gagal mengirim email:", error);
      toast.error("Error Generating user!");
    } finally {
      setLoadingItemId(null);
    }
  };

  const handleClosePopup = () => {
    setSelectedUser(null);
    setIsAllergicPopUp(false);
  };

  const filteredUsers =
    activeTab === "hadir"
      ? users.filter(
          (user: any) => user.registrationAttendance == "Yes" && !user.newData
        )
      : activeTab === "tidak-hadir"
      ? users.filter(
          (user: any) => user.registrationAttendance != "Yes" && !user.newData
        )
      : users.filter((user: any) => user.newData == true);

  const totalPax = filteredUsers.reduce((total: any, user: any) => {
    if (user.numberOfPax == null) {
      return "0";
    }
    const paxNumber = parseInt(user.numberOfPax.replace("pax", ""), 10);
    return total + (isNaN(paxNumber) ? 0 : paxNumber);
  }, 0);

  const exportToExcel = async () => {
    const data = await Promise.all(
      filteredUsers.map(async (user: any, index: any) => {
        const allergicDetails =
          user.isAllergic === "Yes" ? await fetchAllergicDetails(user.id) : [];
        const allergicInfo = allergicDetails
          .map(
            (detail: any, idx: any) =>
              `[${idx + 1}] ${detail.name}: ${detail.allergic}  `
          )
          .join("\n");

        return {
          No: index + 1,
          Nama: user.fullName,
          "Company Name": user.companyName,
          Email: user.newData ? "no data" : user.email,
          "Phone Number": user.newData ? "no data" : user.phoneNumber,
          Pax: user.numberOfPax ? user.numberOfPax.replace("pax", "") : "0",
          "List Allergic": allergicInfo || "No Allergic",
          Kehadiran: user.registrationAttendance,
          Link: `https://rsvp-hfi10th-anniversary.vercel.app/?id=${user.phoneNumber}`,
        };
      })
    );

    // Urutkan data berdasarkan companyName
    const sortedData = data.sort((a, b) =>
      a["Company Name"].localeCompare(b["Company Name"])
    );

    const worksheet = XLSX.utils.json_to_sheet(sortedData);
    const workbook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workbook, worksheet, "Users");

    const excelBuffer = XLSX.write(workbook, {
      bookType: "xlsx",
      type: "array",
    });

    const dataBlob = new Blob([excelBuffer], {
      type: "application/octet-stream",
    });

    saveAs(dataBlob, `user_registration-${activeTab}.xlsx`);
  };

  const [protocolAndDomain, setProtocolAndDomain] = useState(""); // State untuk menyimpan protokol dan domain

  const getProtocolAndDomain = () => {
    const protocolAndDomain =
      window.location.protocol + "//" + window.location.host;
    setProtocolAndDomain(protocolAndDomain);
  };

  // Fungsi untuk mengubah halaman
  const changePage = (page: number) => {
    setCurrentPage(page);
  };

  // Filter data berdasarkan halaman yang aktif
  const currentPageData = filteredUsers.slice(
    (currentPage - 1) * pageSize,
    currentPage * pageSize
  );

  const totalPages = Math.ceil(filteredUsers.length / pageSize);

  useEffect(() => {
    getProtocolAndDomain();
  }, []);

  return (
    <div className="flex">
      <Navbar />

      <div className="container w-10/12  p-6 ">
        <Head>
          <title>User Registration</title>
        </Head>
        <ToastContainer />
        <h1 className="text-3xl font-bold mb-1">User Registration</h1>

        <div className=" flex justify-between">
          <div>
            <button
              className={`mr-2 p-2 rounded-md focus:outline-none ${
                activeTab === "hadir"
                  ? "bg-indigo-600 text-white"
                  : "bg-gray-200 text-gray-600"
              }`}
              onClick={() => {
                setActiveTab("hadir"), setCurrentPage(1);
              }}
            >
              Daftar Hadir
            </button>
            <button
              className={`mr-2 p-2 rounded-md focus:outline-none ${
                activeTab === "tidak-hadir"
                  ? "bg-indigo-600 text-white"
                  : "bg-gray-200 text-gray-600"
              }`}
              onClick={() => {
                setActiveTab("tidak-hadir"), setCurrentPage(1);
              }}
            >
              Daftar Tidak Hadir
            </button>
            <button
              className={`p-2 rounded-md focus:outline-none ${
                activeTab === "data-baru"
                  ? "bg-indigo-600 text-white"
                  : "bg-gray-200 text-gray-600"
              }`}
              onClick={() => {
                setActiveTab("data-baru"), setCurrentPage(1);
              }}
            >
              Data Manual
            </button>
          </div>
          <div>
            <button
              className="p-2 mb-4 ml-3 bg-green-600 text-white rounded-md"
              onClick={exportToExcel}
            >
              Export data {`${activeTab}`}
            </button>
          </div>
        </div>

        {activeTab == "hadir" ? (
          <h1 className="text-2xl font-bold mb-4">Total Pax: {totalPax} Pax</h1>
        ) : (
          ""
        )}
        <div className="mt-4 flex justify-start mb-4">
          {Array.from({ length: totalPages }).map((_, index) => (
            <button
              key={index}
              onClick={() => changePage(index + 1)}
              className={`px-3 py-1 mx-1 rounded-md ${
                currentPage === index + 1
                  ? "bg-indigo-600 text-white"
                  : "bg-gray-200 text-gray-600"
              }`}
            >
              {index + 1}
            </button>
          ))}
        </div>
        <div className="overflow-x-auto shadow-2xl">
          <table className="w-full max-w-none border-collapse  table-fixed min-w-[1920px]">
            <thead>
              <tr>
                <th className="p-3 w-[50px] font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                  {activeTab == "data-baru" ? "ID" : "No"}
                </th>
                <th className="p-3 font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                  Name
                </th>
                <th className="p-3 w-[280px] font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                  Company Name
                </th>
                {activeTab !== "data-baru" && (
                  <>
                    <th className="p-3 w-[300px] font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                      Email
                    </th>
                    <th className="p-3 w-[160px] font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                      Phone Number
                    </th>
                    <th className="p-3 w-[110px] font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                      Allergic
                    </th>
                  </>
                )}
                <th className="p-3 w-[470px] font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                  Link
                </th>
                <th className="p-3 w-[120px] font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                  Generate QR
                </th>
                <th className="p-3 font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                  QR Code
                </th>
              </tr>
            </thead>
            <tbody>
              {currentPageData.map((user: any, i: any) => (
                <tr key={i} className="bg-gray-50">
                  <td className="p-3 border border-gray-300">
                    {activeTab == "data-baru"
                      ? user.phoneNumber
                      : (currentPage - 1) * pageSize + i + 1}
                  </td>
                  <td className="p-3 border border-gray-300">
                    {user.fullName}
                  </td>
                  <td className="p-3 border border-gray-300">
                    {user.companyName}
                  </td>
                  {activeTab !== "data-baru" && (
                    <>
                      <td className="p-3 border border-gray-300">
                        {user.email}
                      </td>
                      <td className="p-3 border border-gray-300">
                        {user.phoneNumber}
                      </td>
                      <td className="p-3 border border-gray-300">
                        {user.isAllergic == "Yes" ? (
                          <div
                            className="underline cursor-pointer"
                            onClick={() => {
                              setSelectedUser(user);
                              fetchAllergicInfo(user.id);
                            }}
                          >
                            See List
                          </div>
                        ) : (
                          <div className="cursor-not-allowed">No</div>
                        )}
                      </td>
                    </>
                  )}
                  <td className="p-3 border border-gray-300">
                    {user.registrationAttendance == "Yes" ? (
                      <Link
                        target="_blank"
                        href={`https://rsvp-hfi10th-anniversary.vercel.app/?id=${user.phoneNumber}`}
                      >
                        https://rsvp-hfi10th-anniversary.vercel.app/?id=
                        {user.phoneNumber}
                      </Link>
                    ) : (
                      <div>No Data</div>
                    )}
                  </td>
                  <td className="p-3 border border-gray-300">
                    {user.registrationAttendance === "Yes" ? (
                      user.qrConfirm ? (
                        <div className="underline cursor-not-allowed text-gray-400">
                          Generated
                        </div>
                      ) : (
                        <button
                          disabled={isLoading}
                          className="underline cursor-pointer text-blue-500"
                          onClick={() => generateQr(user.phoneNumber, i)}
                        >
                          {isLoading == true ? (
                            <h3 className="cursor-not-allowed">Loading</h3>
                          ) : (
                            <h3>Generate</h3>
                          )}
                        </button>
                      )
                    ) : (
                      <div className="cursor-not-allowed text-gray-400">
                        No Data
                      </div>
                    )}
                  </td>
                  <td className="p-3 border border-gray-300 ">
                    {user.qrConfirm ? (
                      <Image
                        className="w-[70px] mx-auto"
                        src={user.qrConfirm}
                        alt="QR"
                        width={1000}
                        height={1000}
                      />
                    ) : (
                      <>Not Generated</>
                    )}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>

        {allergicPopUp && (
          <div className="fixed inset-0 bg-gray-700 bg-opacity-50 flex justify-center items-center">
            <div className="bg-white p-4 rounded-md w-[700px] shadow-2xl popup-animation">
              <h2 className="text-lg font-bold mb-2">Allergic List</h2>
              <h2>From User</h2>
              <div className="mb-6">
                <h2>Name: {selectedUser.fullName}</h2>
                <h2>Company: {selectedUser.companyName}</h2>
                <h2>Phone Number: {selectedUser.phoneNumber}</h2>
              </div>
              <table className="w-full border-collapse">
                <thead>
                  <tr>
                    <th className="p-3 font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                      Name
                    </th>
                    <th className="p-3 font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                      Allergic
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {allergicInfo.map((user: any, i: any) => (
                    <tr key={i} className="bg-gray-50">
                      <td className="p-3 border border-gray-300">
                        {user.name}
                      </td>
                      <td className="p-3 border border-gray-300">
                        {user.allergic}
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>

              <button
                className="mt-4 p-2 bg-indigo-600 text-white rounded-md"
                onClick={() => {
                  setSelectedUser(null);
                  setIsAllergicPopUp(false);
                }}
              >
                Close
              </button>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default IndexPage;
