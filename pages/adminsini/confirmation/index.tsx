import Navbar from "@/components/Navbar";
import { PrismaClient } from "@prisma/client";
import Head from "next/head";
import { FormEvent, useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const prisma = new PrismaClient();

export async function getServerSideProps() {
  const users = await prisma.user.findMany();

  return {
    props: {
      initialUsers: users,
    },
  };
}

const IndexPage = ({ initialUsers }: any) => {
  const [users, setUsers] = useState(initialUsers);
  const [loadingItemId, setLoadingItemId] = useState(null);
  const [activeTab, setActiveTab] = useState("hadir");

  const fetchUsers = async () => {
    try {
      const response = await fetch("/api/fetch-users");
      if (response.ok) {
        const data = await response.json();
        setUsers(data);
      } else {
        console.error("Failed to fetch users:", response.statusText);
      }
    } catch (error) {
      console.error("Failed to fetch users:", error);
    }
  };

  const sendEmailConfirmation = async (
    email: any,
    phoneNumber: any,
    fullName: any,
    companyName: any,
    itemId: any
  ) => {
    try {
      setLoadingItemId(itemId);
      const response = await fetch("/api/contact", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email, phoneNumber, fullName, companyName }),
      });
      if (response.ok) {
        toast.success("Registration successful!");
        await updateSendAttendance(phoneNumber);
        await fetchUsers();
      } else {
        toast.error("Error registering user!");
      }
    } catch (error) {
      console.error("Gagal mengirim email:", error);
      toast.error("Error registering user!");
    } finally {
      setLoadingItemId(null);
    }
  };

  const updateSendAttendance = async (phoneNumber: any) => {
    try {
      const response = await fetch("/api/update-send-attendance", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ phoneNumber }),
      });
      if (!response.ok) {
        console.error("Gagal memperbarui data sendAttendance!");
      }
    } catch (error) {
      console.error("Gagal memperbarui data sendAttendance:", error);
    }
  };

  console.log(users);
  console.log(activeTab);

  const filteredUsers =
    activeTab === "hadir"
      ? users.filter((user: any) => user.confirmAttendance == "Yes")
      : users.filter((user: any) => user.confirmAttendance != "Yes");

  return (
    <div className="flex">
      <Navbar />
      <div className="container mx-auto p-6">
        <Head>
          <title>User Confirmation</title>
        </Head>
        <ToastContainer />
        <h1 className="text-3xl font-bold mb-4">User Confirmation</h1>

        <div className="mb-4">
          <button
            className={`mr-2 p-2 rounded-md focus:outline-none ${
              activeTab === "hadir"
                ? "bg-indigo-600 text-white"
                : "bg-gray-200 text-gray-600"
            }`}
            onClick={() => setActiveTab("hadir")}
          >
            Daftar Hadir
          </button>
          <button
            className={`p-2 rounded-md focus:outline-none ${
              activeTab === "tidak-hadir"
                ? "bg-indigo-600 text-white"
                : "bg-gray-200 text-gray-600"
            }`}
            onClick={() => setActiveTab("tidak-hadir")}
          >
            Daftar Tidak Hadir
          </button>
        </div>

        <table className="w-full border-collapse">
          <thead>
            <tr>
              <th className="p-3 font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                Name
              </th>
              <th className="p-3 font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                Company Name
              </th>
              <th className="p-3 font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                Email
              </th>
              <th className="p-3 font-bold uppercase bg-gray-100 text-gray-600 border border-gray-300 hidden lg:table-cell">
                Phone Number
              </th>
            </tr>
          </thead>
          <tbody>
            {filteredUsers.map((user: any, i: any) => (
              <tr key={i} className="bg-gray-50">
                <td className="p-3 border border-gray-300">{user.fullName}</td>
                <td className="p-3 border border-gray-300">
                  {user.companyName}
                </td>
                <td className="p-3 border border-gray-300">{user.email}</td>
                <td className="p-3 border border-gray-300">
                  {user.phoneNumber}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default IndexPage;
