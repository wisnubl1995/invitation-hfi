// pages/register.js

import { useState } from "react";

import Image from "next/image";
import hero from "@/public/img/hero.png";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Head from "next/head";
import logo from "@/public/img/logohfi.png";
import text1 from "@/public/img/confirm1.png";
import text2 from "@/public/img/text2.png";
import text3 from "@/public/img/text3.png";
import text4 from "@/public/img/text4.png";
import h1 from "@/public/img/h1.png";
import p from "@/public/img/p.png";

export default function Register() {
  const [fullName, setFullName] = useState("");
  const [companyName, setCompanyName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [email, setEmail] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const [isRegister, setIsRegister] = useState(false);

  return (
    <div className="max-w-[480px] mx-auto flex flex-col h-[100vh] bg-main bg-no-repeat bg-cover">
      <Head>
        <title>Invitation HFI</title>
      </Head>
      {/* Hero Section */}
      <ToastContainer />
      {/* Registration Form */}
      <div className="w-full mx-auto text-white p-10">
        <>
          <div className="flex flex-col items-center  gap-10 h-[80vh] justify-center">
            <div className="w-full flex flex-col items-center justify-center">
              <div className="lg:pt-16 w-full flex flex-col items-center">
                <div className="w-full pb-1 flex flex-col items-center">
                  <Image src={text1} alt="logo" />
                </div>
              </div>
              <div className="w-full font-sans  flex flex-col items-center lg:pt-16 pt-5">
                <div className="w-full flex flex-col items-center">
                  <div className="w-12/12">
                    <Image src={logo} alt="logo" />
                  </div>
                  <div className="">
                    <Image src={hero} alt="logo" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      </div>
    </div>
  );
}
