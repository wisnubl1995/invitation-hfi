// pages/register.js

import { useState } from "react";
import { PrismaClient } from "@prisma/client";
import Image from "next/image";
import hero from "@/public/img/hero.png";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Head from "next/head";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import logo from "@/public/img/logohfi.png";
import text from "@/public/img/TEXT.png";
import date from "@/public/img/date.png";
import location from "@/public/img/location.png";
import address from "@/public/img/address.png";

export default function Index() {
  const [fullName, setFullName] = useState("");
  const [companyName, setCompanyName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [email, setEmail] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const [isRegister, setIsRegister] = useState(false);
  const [selectedCountry, setSelectedCountry] = useState("");

  const [attendance, setAttendance] = useState("");

  console.log(attendance);

  const handleSubmit = async (e: any) => {
    setIsLoading(true);
    e.preventDefault();
    try {
      const response = await fetch("/api/update-attendance", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          fullName,
          companyName,
          phoneNumber,
          attendance,
        }),
      });
      if (response.ok) {
        console.log("Thank you for your confirmation!");
        toast.success("Thank you for your confirmation!");
        setTimeout(() => {
          window.location.href = "/confirmation/success";
        }, 900);
      } else {
        console.error("Error registering user:", response.statusText);
        toast.error("Your phone number is not registered!");
      }
    } catch (error) {
      console.error("Error registering user:", error);
      toast.error("Error while confirmation!");
    } finally {
      setIsLoading(false); // Set loading status back to false after request completes
    }
  };

  return (
    <div className="max-w-[480px] mx-auto flex flex-col justify-center h-[100vh] bg-main bg-no-repeat bg-cover">
      <Head>
        <title>Invitation HFI</title>
      </Head>
      {/* Hero Section */}
      <ToastContainer />
      {/* Registration Form */}
      <div className="w-full mx-auto text-white p-10">
        {isRegister == true ? (
          <div className="flex flex-col items-center h-[80vh] justify-center">
            <div className="w-full">
              <div className="w-full flex flex-col items-center">
                <div className="w-12/12">
                  <Image src={logo} alt="logo" />
                </div>
                <div className="lg:pt-3 pt-0">
                  <Image src={hero} alt="logo" />
                </div>
              </div>
              <form
                onSubmit={handleSubmit}
                className="text-[#ffffff] lg:pt-10 pt-3"
              >
                <h1 className="uppercase lg:text-[28px] text-[20px] text-center pb-3">
                  Confirmation Form
                </h1>
                <div className="mb-4">
                  <label
                    htmlFor="fullName"
                    className="block text-sm font-medium pl-3 text-[20px] text-center"
                  >
                    Full Name
                  </label>
                  <input
                    type="text"
                    id="fullName"
                    value={fullName}
                    onChange={(e) => setFullName(e.target.value)}
                    required
                    className="mt-1 p-2 block w-full rounded-full border-white border-[1px] bg-transparent"
                  />
                </div>
                <div className="mb-4">
                  <label
                    htmlFor="companyName"
                    className="block text-sm font-medium pl-3 text-[20px] text-center"
                  >
                    Company Name
                  </label>
                  <input
                    type="text"
                    id="companyName"
                    value={companyName}
                    onChange={(e) => setCompanyName(e.target.value)}
                    required
                    className="mt-1 p-2 block w-full rounded-full border-white border-[1px] bg-transparent"
                  />
                </div>
                <div className="mb-4">
                  <label
                    htmlFor="phoneNumber"
                    className="block text-sm font-medium pl-3 text-[20px] text-center"
                  >
                    Phone Number
                  </label>
                  <div className="flex mt-1 px-3 w-full py-1 rounded-full border-white border-[1px] bg-transparent  text-black">
                    <PhoneInput
                      country={"id"}
                      value={phoneNumber}
                      onChange={(phone: any) => setPhoneNumber(phone)}
                      enableSearch={true}
                      containerStyle={{ backgroundColor: "transparent" }}
                      buttonStyle={{
                        backgroundColor: "#0000",
                        opacity: "0.0 - 1.0;",
                        borderStyle: "none",
                      }}
                      inputStyle={{
                        backgroundColor: "#0000",
                        borderStyle: "none",
                        color: "white",
                      }}
                      dropdownStyle={{
                        borderRadius: "20px",
                      }}
                    />
                  </div>
                </div>
                <div className="mb-4">
                  <span className="block text-sm font-medium pl-3 text-[20px] text-center">
                    Attendance
                  </span>
                  <div className="mt-1 w-full  justify-center items-center flex pt-3 gap-16">
                    <label
                      htmlFor="attendanceYes"
                      className="inline-flex items-center mr-6"
                    >
                      <input
                        type="radio"
                        id="attendanceYes"
                        name="attendance"
                        value="Yes"
                        checked={attendance === "Yes"}
                        onChange={(e) => setAttendance(e.target.value)}
                        className="form-radio h-5 w-5 text-blue-600"
                      />
                      <span className="ml-2 text-sm">YES</span>
                    </label>
                    <label
                      htmlFor="attendanceNo"
                      className="inline-flex items-center"
                    >
                      <input
                        type="radio"
                        id="attendanceNo"
                        name="attendance"
                        value="No"
                        checked={attendance === "No"}
                        onChange={(e) => setAttendance(e.target.value)}
                        className="form-radio h-5 w-5 text-red-600"
                      />
                      <span className="ml-2 text-sm">NO</span>
                    </label>
                  </div>
                </div>

                <div className="flex justify-center lg:pt-10 pt-3">
                  <button
                    type="submit"
                    className=" font-sans px-6 py-1 bg-gradient-to-b from-[#D9D9D9] to-[#737373] border-white border-2 rounded-full w-10/12 text-[24px] font-bold tracking-wider hover:bg-gradient-to-b hover:from-[#737373] hover:to-[#D9D9D9] transition-all ease-out duration-200"
                    disabled={isLoading} // Disable button when loading
                  >
                    {isLoading ? "Loading..." : "SUBMIT"}{" "}
                    {/* Change button text when loading */}
                  </button>
                </div>
              </form>
            </div>
          </div>
        ) : (
          <>
            <div className="flex flex-col items-center  gap-10 h-[80vh]  justify-center">
              <div className="w-full flex flex-col items-center">
                <div className="w-12/12 ">
                  <Image src={logo} alt="logo" />
                </div>
                <div className="">
                  <Image src={hero} alt="logo" />
                </div>
              </div>

              <div className="w-full flex flex-col items-center justify-center text-center ">
                <h1 className="lg:text-[32px] text-[20px]  font-bold bg-gradient-to-b from-[#FFFFFF] to-[#999999] inline-block text-transparent bg-clip-text">
                  CONFIRMATION FORM
                </h1>
                <p className="lg:text-[20px] text-[14px]">
                  We are delighted to confirm your attendance at the Gala Dinner
                  on [Date] at [Venue Name]. Your presence will be a valued
                  addition to this special evening.
                </p>
              </div>

              <div className="w-full flex flex-col items-center justify-center text-center">
                <h1 className="lg:text-[32px] text-[20px]">Event Details:</h1>
                <p className="lg:text-[20px] text-[14px]">
                  Date: [Date]
                  <br />
                  Time: [Time]
                  <br />
                  Venue: [Venue Name], [Venue Address] <br />
                  Dress Code: [Dress Code, if applicable]
                </p>
              </div>

              <div className="w-full font-sans  flex flex-col items-center lg:pt-10 pt-3">
                <button
                  onClick={() => {
                    setIsRegister(true);
                  }}
                  className="px-6 py-1 bg-gradient-to-b from-[#D9D9D9] to-[#737373] border-white border-2 rounded-full w-10/12 text-[24px] font-bold tracking-wider hover:bg-gradient-to-b hover:from-[#737373] hover:to-[#D9D9D9] transition-all ease-out duration-200"
                >
                  CONFIRMATION
                </button>
              </div>
            </div>
          </>
        )}
      </div>
    </div>
  );
}
