/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  i18n: {
    locales: ["id", "en"], // Bahasa yang didukung
    defaultLocale: "id", // Set default locale ke Bahasa Indonesia
  },
};

export default nextConfig;
