import React, { useState } from "react";

type Props = {};

const Navbar = (props: Props) => {
  const [isMenuVisible, setIsMenuVisible] = useState(true);

  const toggleMenuVisibility = () => {
    setIsMenuVisible(!isMenuVisible);
  };

  return (
    <div className="w-2/12">
      <div className="fixed">
        <button
          onClick={toggleMenuVisibility}
          className="bg-gray-800 text-white py-2 px-4 rounded-md mt-4 ml-4"
        >
          {isMenuVisible ? "Hide Menu" : "Show Menu"}
        </button>
        {isMenuVisible && (
          <nav className="w-38 bg-gray-800 h-screen mt-4">
            <div className="flex flex-col justify-between h-full py-6 px-4">
              <div>
                <h2 className="text-lg font-semibold text-white mb-4">Menu</h2>
                <ul className="space-y-2">
                  <li>
                    <a
                      href="/adminsini/registration"
                      className="text-gray-300 hover:text-white hover:bg-gray-700 py-2 px-4 rounded-md block"
                    >
                      User Registration
                    </a>
                  </li>
                  <li>
                    <a
                      href="/adminsini/seating"
                      className="text-gray-300 hover:text-white hover:bg-gray-700 py-2 px-4 rounded-md block"
                    >
                      Manage User Seat
                    </a>
                  </li>
                  <li>
                    <a
                      href="/adminsini/final"
                      className="text-gray-300 hover:text-white hover:bg-gray-700 py-2 px-4 rounded-md block"
                    >
                      List Final Users
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        )}
      </div>
    </div>
  );
};

export default Navbar;
