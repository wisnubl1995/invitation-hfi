import React from "react";
import { useTranslation } from "react-i18next";

type Props = {};

const Agendabox = (props: Props) => {
  const { t, i18n } = useTranslation();
  return (
    <div className="flex flex-col  py-3 px-3 mt-10 bg-[#5C0406] shadow-box w-10/12 text-white rounded-xl">
      <div className="text-center">
        <h3 className="uppercase text-[26px]">{t("rundown")}</h3>
      </div>

      <div className="flex justify-center">
        <div className="h-[1px] w-11/12 bg-white "></div>
      </div>

      <div className="flex flex-col items-start text-left px-3">
        <div className="mt-2">
          <h3 className="text-[10px] font-bold">17:00 - 18:00</h3>
          <h3 className="text-[13px] text-white/75 font-light -mt-1">
            {t("registration")}
          </h3>
        </div>
        <div className="mt-2">
          <h3 className="text-[10px] font-bold">18:00 - 18:15</h3>
          <h3 className="text-[13px] text-white/75 font-light -mt-1">
            {t("brief")}
          </h3>
        </div>
        <div className="mt-2">
          <h3 className="text-[10px] font-bold">18:15 - 18:20</h3>
          <h3 className="text-[13px] text-white/75 font-light -mt-1">
            {t("anthem")}
          </h3>
        </div>
        <div className="mt-2">
          <h3 className="text-[10px] font-bold">18:20 - 18:30</h3>
          <h3 className="text-[13px] text-white/75 font-light -mt-1">
            {t("welcoming")}
          </h3>
        </div>
        <div className="mt-2">
          <h3 className="text-[10px] font-bold">18:30 - 18:40</h3>
          <h3 className="text-[13px] text-white/75 font-light -mt-1">
            {t("caleidoscope")}
          </h3>
        </div>
        <div className="mt-2">
          <h3 className="text-[10px] font-bold">18:40 - 19:05</h3>
          <h3 className="text-[13px] text-white/75 font-light -mt-1">
            {t("opening")}
          </h3>
        </div>
        <div className="mt-2">
          <h3 className="text-[10px] font-bold">19:05 - 19:10</h3>
          <h3 className="text-[13px] text-white/75 font-light -mt-1">
            {t("toast")}
          </h3>
        </div>
        <div className="mt-2">
          <h3 className="text-[10px] font-bold">19:10 - 19:55</h3>
          <h3 className="text-[13px] text-white/75 font-light -mt-1">
            {t("dinner")}
          </h3>
        </div>
        <div className="mt-2">
          <h3 className="text-[10px] font-bold">19:55 - 20:10</h3>
          <h3 className="text-[13px] text-white/75 font-light -mt-1">
            {t("cultural")}
          </h3>
        </div>
        <div className="mt-2">
          <h3 className="text-[10px] font-bold">20:10 - 20:25</h3>
          <h3 className="text-[13px] text-white/75 font-light -mt-1">
            {t("reward")}
          </h3>
        </div>
        <div className="mt-2">
          <h3 className="text-[10px] font-bold">20:25 - 20:55</h3>
          <h3 className="text-[13px] text-white/75 font-light -mt-1">
            {t("special")}
          </h3>
        </div>
        <div className="mt-2">
          <h3 className="text-[10px] font-bold">20:55 - 21:00</h3>
          <h3 className="text-[13px] text-white/75 font-light -mt-1">
            {t("closing")}
          </h3>
        </div>
      </div>
    </div>
  );
};

export default Agendabox;
