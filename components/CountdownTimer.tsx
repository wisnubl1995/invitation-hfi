import { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";

const CountdownTimer = () => {
  const [timeLeft, setTimeLeft] = useState({
    days: 0,
    hours: 0,
    minutes: 0,
    seconds: 0,
  });
  const { t, i18n } = useTranslation();
  const [hydrated, setHydrated] = useState(false); // untuk memastikan hanya rendering di klien

  useEffect(() => {
    setHydrated(true); // set hydrated to true untuk menandakan bahwa komponen telah dirender di klien

    const calculateTimeLeft = () => {
      const difference = +new Date("2024-07-11T18:00:00+07:00") - +new Date();
      let timeLeft: any = {};

      if (difference > 0) {
        timeLeft = {
          days: Math.floor(difference / (1000 * 60 * 60 * 24)),
          hours: Math.floor((difference / (1000 * 60 * 60)) % 24),
          minutes: Math.floor((difference / 1000 / 60) % 60),
          seconds: Math.floor((difference / 1000) % 60),
        };
      } else {
        timeLeft = { days: 0, hours: 0, minutes: 0, seconds: 0 };
      }

      return timeLeft;
    };

    const timer = setInterval(() => {
      setTimeLeft(calculateTimeLeft());
    }, 1000);

    return () => clearInterval(timer);
  }, []);

  if (!hydrated) {
    // Hanya menampilkan loading di server side
    return null;
  }

  return (
    <div className="flex flex-col items-center mt-6  shadow-box w-12/12 text-white rounded-xl font-sans">
      <div className="flex justify-center w-full space-x-2">
        <div className="flex flex-col items-center bg-[#5C0406] rounded-md w-4/12 px-5  py-2 text-black">
          <span className="text-md font-bold text-white">{timeLeft.days}</span>
          <span className="text-[10px] -mt-1 uppercase text-[#A5DEFF]">
            {t("days")}
          </span>
        </div>
        <div className="flex flex-col items-center bg-[#5C0406] rounded-md w-4/12 px-5  py-2 text-black">
          <span className="text-md font-bold text-white">{timeLeft.hours}</span>
          <span className="text-[10px] -mt-1 uppercase text-[#A5DEFF]">
            {t("hours")}
          </span>
        </div>
        <div className="flex flex-col items-center bg-[#5C0406] rounded-md w-4/12 px-5  py-2 text-black">
          <span className="text-md font-bold text-white">
            {timeLeft.minutes}
          </span>
          <span className="text-[10px] -mt-1 uppercase text-[#A5DEFF]">
            {t("minutes")}
          </span>
        </div>
        <div className="flex flex-col items-center bg-[#5C0406] rounded-md w-4/12 px-5  py-2 text-black">
          <span className="text-md font-bold text-white">
            {timeLeft.seconds}
          </span>
          <span className="text-[10px] -mt-1 uppercase text-[#A5DEFF]">
            {t("seconds")}
          </span>
        </div>
      </div>
    </div>
  );
};

export default CountdownTimer;
